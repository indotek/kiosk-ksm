var el = $(".stepper");
var _me = el;
var _targetId = getTargetId(_me);
var _tabItems = getTabItems(_me);
var _elTarget = getElementTarget(_targetId);
var _elTargetContent = getElementTargetContent(_elTarget);
if (StringUtils.isBlank(window.location.hash) || window.location.hash != "#withnik") {
	localStorage.removeItem("data-nik");
}

function getTargetId(el) {
	return el.attr("data-target");
};

function getTabItems(el) {
	return el.find("li a");
};

function getElementTarget(targetId) {
	return $(".stepper-container-content" + targetId);
};

function getElementTargetContent(elementTarget) {
	return elementTarget.children(".stepper-content");
};

function renderSelect(target, data, value, label) {
	data.forEach(function (r) {
		var createOption = $("<option></option>");
		createOption.attr("value", r[value]);
		createOption.html(r[label]);

		target.append(createOption);
	});
}

function showPopUpAfterRegister(result, nik) {
	var modal = $("#modalRegistration");

	modal.find(".modal-icon").removeClass("animated rotateIn");
	modal.find("#modal-message").addClass("text-center");
	modal.find("form#formPayment").addClass("d-none");
	modal.find("#btnAccept").addClass("d-none");
	modal.find("#btnDecline").html("Tutup");
	modal.find("#btnDecline").off("click");
	modal.find("#btnAccept").off("click");

	if (result.code == "0") {
		_elTarget.find(".stepper-processor #btnCancel").trigger("click");

		modal.find(".modal-dialog").removeClass("modal-danger").addClass("modal-success");
		modal.find(".modal-title").html(langPopupRegistration["success"]["title"]);
		modal.find(".modal-icon").removeClass("fa-times").addClass("fa-check");
		modal.find("#modal-message").html(langPopupRegistration["success"]["message"]);
	} else {
		modal.find(".modal-dialog").addClass("modal-danger").removeClass("modal-success");
		modal.find(".modal-title").html(langPopupRegistration["failed"]["title"]);
		modal.find(".modal-icon").addClass("fa-times").removeClass("fa-check");
		modal.find("#modal-message").html(langPopupRegistration["failed"]["message"]);
	}
	modal.find(".modal-icon").addClass("animated rotateIn");

	modal.find("#btnDecline").on("click", function (ev) {
		if (result.code == "0") {
			API.customer.search({nik:nik}, function (resultCustomerSearch) {
				if(resultCustomerSearch.code == 0) {
					localStorage.setItem("data-customer", JSON.stringify(resultCustomerSearch.data));

					window.location.href = site_url + "pages/customer/search#detail";
				}
			});
		}

		modal.modal("hide");
	});

	modal.modal({show: true, backdrop: 'static', keyboard: false});
}

function showPopUpPreRegister() {
	var modal = $("#modalRegistration");

	modal.find(".modal-icon").removeClass("animated rotateIn");
	modal.find("#modal-message").removeClass("text-center");
	modal.find("form#formPayment").removeClass("d-none");
	modal.find("button#btnAccept").removeClass("d-none");
	modal.find("#btnAccept").off("click");
	modal.find("#btnDecline").off("click");

	modal.find(".modal-dialog").removeClass("modal-danger").addClass("modal-success");
	modal.find(".modal-title").html(langPopupRegistration["tnc"]["title"]);
	modal.find(".modal-icon").removeClass("fa-times").removeClass("fa-check");
	var msg = langPopupRegistration["tnc"]["message"];
	msg = msg.replace("%a", simpananPokokWajib["pokok"]["description"]);
	msg = msg.replace("%b", simpananPokokWajib["wajib"]["description"]);
	modal.find("#modal-message").html(msg);

	var valWajib = simpananPokokWajib["wajib"]["initial_deposit"];
	// if(valWajib.indexOf(".") == -1) valWajib += ".00";

	modal.find("#input-nominal-simpanan-wajib").val(valWajib).prop("readonly", true);


	modal.find("#btnDecline").on("click", function (ev) {
		modal.modal("hide");
	});
	modal.find("#btnAccept").on("click", function (ev) {
		ev.preventDefault();

		var dataSimpanan = {
			pokok : removeCurrencyFormat(modal.find("form#formPayment #input-nominal-simpanan-pokok").val()),
			wajib : removeCurrencyFormat(modal.find("form#formPayment #input-nominal-simpanan-wajib").val()),
		}

		_elTarget.find(".stepper-processor #btnProcess").data("saving", dataSimpanan);

		modal.modal("hide");
	});

	modal.modal({show: true, backdrop: 'static', keyboard: false});
}

showPopUpPreRegister();
// showPopUpAfterRegister({code:1});

$.fn.extend({
	formValidator: function () {
		var arrRules = {};

		$(this).find(":input").each(function () {
			var data = $(this).data();
			var rules = {};
			for (var d in data) {
				if (d.indexOf("validation") != -1) {
					var d2 = d.replace("validation", "");
					d2 = d2.substr(0, 1).toLowerCase() + d2.substr(1, d.length);

					if (data[d]) {
						if (data[d] == "true") {
							rules[d2] = true;
						} else if (data[d] == "false") {
							rules[d2] = false;
						} else {
							try {
								rules[d2] = JSON.parse(data[d]);
							} catch (e) {
								rules[d2] = data[d];
							}
						}
					} else {
						rules[d2] = true;
					}
				}
			}

			if (Object.keys(rules).length > 0) {
				arrRules[$(this).attr("id")] = rules;
			}
		});

		var settings = {};
		if (Object.keys(arrRules).length > 0) settings["rules"] = arrRules;
		var validator = $(this).validate(settings);

		return validator;
	}
});

// el.each(function () {


	_elTargetContent.hide();

	_elTargetContent.children("form").each(function () {
		$(this).formValidator();
	});

	_tabItems.on("click touch", function (ev, type) {
		ev.preventDefault();

		var _me2 = $(this);
		var _tabItems = getTabItems(_me);
		var _iCurrentTab = _tabItems.index($(this));
		var _elTargetContent = getElementTargetContent(_elTarget);
		var _elActive = _tabItems.parent("li.active").children("a");
		var _iActive = _tabItems.index(_elActive);

		var form = null;

		if (_iActive != -1) {
			form = $(_elTargetContent.get(_iActive)).find("form");
		}

		if (_elActive.length > 0 && _iActive < _iCurrentTab) {
			var validator = form.formValidator();

			if (!validator.form()) {
				return;
			}
		}

		_tabItems.parent().removeClass("completed active");
		_elTargetContent.removeClass("in show").hide();
		_tabItems.each(function (i) {
			if (i < _iCurrentTab + 1) {
				if (i == _iCurrentTab) {
					$(this).parent().addClass("active");
				} else {
					$(this).parent().addClass("completed");
				}
			}
		});
		$(_elTargetContent.get(_iCurrentTab)).show().addClass("in show");

		var _elNewActive = _tabItems.parent("li.active").children("a");
		var _iNewActive = _tabItems.index(_elNewActive);
		_elTarget.find(".stepper-navigator button").removeClass("in show").hide();
		if (_iNewActive < _tabItems.length - 1) {
			_elTarget.find(".stepper-navigator #btnNext").show().addClass("in show");
		}
		if (_iNewActive > 0) {
			_elTarget.find(".stepper-navigator #btnBack").show().addClass("in show");
		}
		_elTarget.find(".stepper-processor").removeClass("in show").hide();
		if (_iNewActive == _tabItems.length - 1) {
			_elTarget.find(".stepper-processor").show().addClass("in show");
		} else {
			_elTarget.find(".stepper-processor").removeClass("in show").hide();
		}

		// if (_iActive != -1) {
		// console.log(_iActive, _iNewActive);
		// }
	});

	_elTarget.find(".stepper-navigator #btnBack").on("click touch", function (ev) {
		ev.preventDefault();
		var _me2 = $(this);
		var _tabItems = getTabItems(_me);
		var _tabItemActive = _tabItems.parent("li.active");

		_tabItemActive.prev().children("a").trigger("click", ["back"]);
	});
	_elTarget.find(".stepper-navigator #btnNext").on("click touch", function (ev) {
		ev.preventDefault();
		var _me2 = $(this);
		var _tabItems = getTabItems(_me);
		var _tabItemActive = _tabItems.parent("li.active");
		_tabItemActive.next().children("a").trigger("click", ["next"]);
	});
	_elTarget.find(".stepper-processor #btnCancel").on("click touch", function () {
		var _me2 = $(this);
		var _tabItems = getTabItems(_me);
		var _tabItemActive = _tabItems.parent("li");
		var _elTargetContent = getElementTargetContent(_elTarget);

		_elTargetContent.find("form").each(function () {
			$(this).form("clear");
		});
		_tabItemActive.eq(0).children("a").trigger("click", ["next"]);
	});
	_elTarget.find(".stepper-processor #btnProcess").on("click touch", function () {
		var _me2 = $(this);
		var _tabItems = getTabItems(_me);
		var _tabItemActive = _tabItems.parent("li.active");
		var _elTargetContent = getElementTargetContent(_elTarget);

		var data = {};

		_elTargetContent.find("form").each(function () {
			data[$(this).clear_key()] = $(this).form("get");
		});

		if(_me2.data("saving") != undefined && _me2.data("saving") != null) {
			data["saving"] = _me2.data("saving");
		}

		API.customer.registration(data, function (result) {
			showPopUpAfterRegister(result, data["personalData"]["nik"]);
		}, null);
	});

	_elTargetContent.find("#select-province").on("change", function (ev) {
		ev.preventDefault();

		var _groupAddress = $(this).parents(".row");


		_groupAddress.find("#select-village option:not(:first)").remove();
		_groupAddress.find("#select-village").trigger("change");

		_groupAddress.find("#select-district option:not(:first)").remove();
		_groupAddress.find("#select-district").trigger("change");

		_groupAddress.find("#select-regency option:not(:first)").remove();

		if (StringUtils.isNotBlank($(this).val())) {
			API.master.regency.list({provinceId: $(this).val()}, function (result) {
				if (result.code == "0" && result.data != undefined && Array.isArray(result.data) && result.data.length > 0) {
					renderSelect(_groupAddress.find("#select-regency"), result.data, "id", "name");
				}
			}, null);
		}

		_groupAddress.find("#select-regency").trigger("change");
	});

	_elTargetContent.find("#select-regency").on("change", function (ev) {
		ev.preventDefault();

		var _groupAddress = $(this).parents(".row");

		_groupAddress.find("#select-village option:not(:first)").remove();
		_groupAddress.find("#select-village").trigger("change");

		_groupAddress.find("#select-district option:not(:first)").remove();

		if (StringUtils.isNotBlank($(this).val())) {
			API.master.district.list({regencyId: $(this).val()}, function (result) {
				if (result.code == "0" && result.data != undefined && Array.isArray(result.data) && result.data.length > 0) {
					renderSelect(_groupAddress.find("#select-district"), result.data, "id", "name");
				}
			}, null);
		}

		_groupAddress.find("#select-district").trigger("change");
	});

	_elTargetContent.find("#select-district").on("change", function (ev) {
		ev.preventDefault();

		var _groupAddress = $(this).parents(".row");

		_groupAddress.find("#select-village option:not(:first)").remove();

		if (StringUtils.isNotBlank($(this).val())) {
			API.master.village.list({districtId: $(this).val()}, function (result) {
				if (result.code == "0" && result.data != undefined && Array.isArray(result.data) && result.data.length > 0) {
					renderSelect(_groupAddress.find("#select-village"), result.data, "id", "name");
				}
			}, null);
		}

		_groupAddress.find("#select-village").trigger("change");
	});

	_tabItems.first().trigger("click");

	if (StringUtils.isNotBlank(localStorage.getItem("data-nik"))) {
		_elTargetContent.find("#input-nik").val(localStorage.getItem("data-nik")).trigger("change");
	}
// });
