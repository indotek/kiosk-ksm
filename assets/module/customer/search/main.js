var validateField = function validateField($input) {
	if ($input.hasClass('validate')) {
		var value = $input.val();
		var noValue = !value.length;
		var isValid = !$input[0].validity.badInput;

		if (noValue && isValid) {
			$input.removeClass('valid').removeClass('invalid');
		} else {
			var valid = $input.is(':valid');
			var length = Number($input.attr('length')) || 0;

			if (valid && (!length || length > value.length)) {
				$input.removeClass('invalid').addClass('valid');
			} else {
				$input.removeClass('valid').addClass('invalid');
			}
		}
	}
};


var sectionDetailCustomer = $("#sectionDetailCustomer");
var sectionSearch = $("#sectionSearchCustomer");
var modalTransaction = $("#modalTransaction");
var modalJoinSaving = $("#modalJoinSaving");
var modalSearchCustomer = $("#modalSearchCustomer");
$("body main").after(modalSearchCustomer);

var tableSaving = sectionDetailCustomer.find('#tableSaving');
var tableLoan = sectionDetailCustomer.find('#tableLoan');
var tableTransaction = modalTransaction.find('#tableTransaction');

var dataTableSaving = tableSaving.DataTable({
	dom: "<'row'<'col-sm-12 col-md-3'l><'col-sm-12 col-md-6'><'col-sm-12 col-md-3'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
	lengthChange: false,
	searching: false,
	scrollY: '50vh',
	scrollCollapse: true,
	paging: false,
	autoWidth: true,
	"columns": [
		{
			"data": "namaSimpanan",
		},
		{
			"data": "saldo",
			"render": function (data, type, row, meta) {
				return currencyFormat(row.saldo);
			}
		},
		{
			"data": "depositAwal",
			"render": function (data, type, row, meta) {
				return currencyFormat(row.depositAwal);
			}
		},
		{
			"data": "sisaSaldo",
			"render": function (data, type, row, meta) {
				return currencyFormat(row.sisaSaldo);
			}
		},
	],
	drawCallback: function (settings) {
		// alert( 'DataTables has redrawn the table' );
	},
	initComplete: function (settings, json) {
		var $wrapper = tableSaving.parents(".dataTables_wrapper");
		var $wrapper_filter = $wrapper.find(".dataTables_filter");
		$wrapper.find('label').each(function () {
			$(this).parent().append($(this).children());
			if ($(this).next().is("input")) {
				$(this).parent().addClass("md-form");
			}
		});
		$wrapper_filter.find('input').each(function (i) {
			$(this).attr("id", "txt-search-" + i);
			$(this).removeClass('form-control-sm');
			var label = $(this).prev();
			label.attr("for", "txt-search-" + i);
			label.insertAfter($(this));
		});

		$wrapper_filter.delegate('input', "focus", function (e) {
			$(this).parent().find("label, i").addClass('active');
		});
		$wrapper_filter.delegate('input', "focusout", function (e) {
			var $this = $(this);
			var noValue = !$this.val();
			var invalid = !e.target.validity.badInput;
			var noPlaceholder = StringUtils.isBlank($this.attr('placeholder'));

			if (noValue && invalid && noPlaceholder) {
				$(this).parent().find("label, i").removeClass('active');
			}

			validateField($this);
		});
		$wrapper.find('select').removeClass('custom-select custom-select-sm form-control form-control-sm');
		$wrapper.find('select').select2({
			minimumResultsForSearch: -1
		});
	}
});

var dataTableTransaction = tableTransaction.DataTable({
	dom: "<'row'<'col-sm-12 col-md-3'l><'col-sm-12 col-md-6'><'col-sm-12 col-md-3'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
	lengthChange: false,
	searching: false,
	scrollY: '50vh',
	scrollCollapse: true,
	paging: false,
	autoWidth: true,
	ordering: false,
	info: false,
	"columns": [
		{
			"width": "40%",
			"data": "namaSimpanan"
		},
		{
			"render": function (data, type, row, meta) {
				// var elInputNominal = '<div class="md-form md-outline mt-0 mb-0">' +
				// 	'<input type="text" id="input-nominal-'+meta.row+'" class="form-control inputmask" name="input-nominal-'+meta.row+'" data-inputmask="\'alias\':\'rp\', \'min\':0" placeholder="0">' +
				// 	'<label for="input-nominal-'+meta.row+'" class="active">Nominal Uang</label>' +
				// '</div>';
				//
				// return elInputNominal;
				return currencyFormat(row.amountToPay);
			}
		},
	],
	drawCallback: function (settings) {

		// alert( 'DataTables has redrawn the table' );
		// console.log(settings, this, );

		// var inputmask = $(this).find(":input.inputmask");
		// if(inputmask.length > 0) {
		// 	inputmask.each(function () {
		// 		var optMask = {
		// 			showMaskOnHover: false,
		// 			showMaskOnFocus: false,
		// 		};
		// 		$(this).inputmask(optMask);
		// 	});
		// }
	},
	initComplete: function (settings, json) {
		var $wrapper = tableSaving.parents(".dataTables_wrapper");
		var $wrapper_filter = $wrapper.find(".dataTables_filter");
		$wrapper.find('label').each(function () {
			$(this).parent().append($(this).children());
			if ($(this).next().is("input")) {
				$(this).parent().addClass("md-form");
			}
		});
		$wrapper_filter.find('input').each(function (i) {
			$(this).attr("id", "txt-search-" + i);
			$(this).removeClass('form-control-sm');
			var label = $(this).prev();
			label.attr("for", "txt-search-" + i);
			label.insertAfter($(this));
		});

		$wrapper_filter.delegate('input', "focus", function (e) {
			$(this).parent().find("label, i").addClass('active');
		});
		$wrapper_filter.delegate('input', "focusout", function (e) {
			var $this = $(this);
			var noValue = !$this.val();
			var invalid = !e.target.validity.badInput;
			var noPlaceholder = StringUtils.isBlank($this.attr('placeholder'));

			if (noValue && invalid && noPlaceholder) {
				$(this).parent().find("label, i").removeClass('active');
			}

			validateField($this);
		});
		$wrapper.find('select').removeClass('custom-select custom-select-sm form-control form-control-sm');
		$wrapper.find('select').select2({
			minimumResultsForSearch: -1
		});
	}
});
var dataTableLoan = tableLoan.DataTable({
	dom: "<'row'<'col-sm-12 col-md-3'l><'col-sm-12 col-md-6'><'col-sm-12 col-md-3'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
	lengthChange: false,
	searching: false,
	scrollY: '50vh',
	scrollCollapse: true,
	paging: false,
	autoWidth: true,
	"columns": [
		{
			"data": "namaSimpanan"
		},
		{
			"data": "saldo"
		},
		{
			"data": "depositAwal"
		},
		{
			"data": "sisaSaldo"
		},
	],
	drawCallback: function (settings) {
		// alert( 'DataTables has redrawn the table' );
	},
	initComplete: function (settings, json) {
		var $wrapper = tableSaving.parents(".dataTables_wrapper");
		var $wrapper_filter = $wrapper.find(".dataTables_filter");
		$wrapper.find('label').each(function () {
			$(this).parent().append($(this).children());
			if ($(this).next().is("input")) {
				$(this).parent().addClass("md-form");
			}
		});
		$wrapper_filter.find('input').each(function (i) {
			$(this).attr("id", "txt-search-" + i);
			$(this).removeClass('form-control-sm');
			var label = $(this).prev();
			label.attr("for", "txt-search-" + i);
			label.insertAfter($(this));
		});

		$wrapper_filter.delegate('input', "focus", function (e) {
			$(this).parent().find("label, i").addClass('active');
		});
		$wrapper_filter.delegate('input', "focusout", function (e) {
			var $this = $(this);
			var noValue = !$this.val();
			var invalid = !e.target.validity.badInput;
			var noPlaceholder = StringUtils.isBlank($this.attr('placeholder'));

			if (noValue && invalid && noPlaceholder) {
				$(this).parent().find("label, i").removeClass('active');
			}

			validateField($this);
		});
		$wrapper.find('select').removeClass('custom-select custom-select-sm form-control form-control-sm');
		$wrapper.find('select').select2({
			minimumResultsForSearch: -1
		});
	}
});

function initPage() {
	sectionSearch.addClass("position-absolute w-100");
	sectionSearch.css({
		"top": "50%",
		"transform": "translateY(-50%)"
	});
}

initPage();

function modalSearchCallback(result, lang) {
	clearDetail();

	if (result.code == 0) {
		window.location.replace(window.location.href + "#detail");
		localStorage.setItem("data-customer", JSON.stringify(result.data));
		showDetail();
	} else {
		modalSearchCustomer.find(".modal-dialog").addClass("modal-danger").removeClass("modal-success");
		modalSearchCustomer.find(".modal-title").html(lang["popupTitle-nik-not-found"]);
		modalSearchCustomer.find(".modal-icon").addClass("fa-times").removeClass("fa-check");
		modalSearchCustomer.find("#modal-message").html(lang["popupMessage-nik-not-found"]);
		modalSearchCustomer.modal("show");
	}

	modalSearchCustomer.find("button#btnConfirmYes").on("click", function (ev) {
		ev.preventDefault();

		localStorage.setItem("data-nik", $("form#formSearchCustomer input#input-nik-search").val());

		window.location.href = site_url + "pages/customer/registration#withnik";
	});
}

function showDetail() {
	sectionSearch.parent().addClass("animated fade fadeOutUp").one($animateCallbackOne, function () {
		sectionSearch.parent().addClass("d-none");
		sectionDetailCustomer.removeClass("d-none").addClass("animated fade fadeInDown").one($animateCallbackOne, function () {
			dataTableSaving.columns.adjust().draw();
			dataTableLoan.columns.adjust().draw();
		});
	});
	fillDetail();
}

function showDetailWithoutAnimation() {
	sectionSearch.parent().addClass("d-none");
	// sectionSearch.parent().addClass("animated fade fadeOutUp").one($animateCallbackOne, function () {
	sectionDetailCustomer.removeClass("d-none").addClass("animated fade fadeInDown").one($animateCallbackOne, function () {
		dataTableSaving.columns.adjust().draw();
		dataTableLoan.columns.adjust().draw();
	});
	// });
	fillDetail();
}

function clearDetail() {
	sectionDetailCustomer.find("span#fill-customer-nik").empty();
	sectionDetailCustomer.find("span#fill-customer-name").empty();
	sectionDetailCustomer.find("span#fill-customer-status").empty();
	sectionDetailCustomer.find("span#fill-customer-created").empty();
	sectionDetailCustomer.find("span#fill-customer-totalBalance").empty();
	sectionDetailCustomer.find("span#fill-customer-totalSaving").empty();
	sectionDetailCustomer.find("span#fill-customer-totalLoan").empty();
}

function fillDetail() {
	var data = JSON.parse(localStorage.getItem("data-customer"));

	sectionDetailCustomer.find("span#fill-customer-nik").html(data.customer.nik);
	sectionDetailCustomer.find("span#fill-customer-name").html(data.customer.name);
	sectionDetailCustomer.find("span#fill-customer-created").html(data.customer.created);
	sectionDetailCustomer.find("span#fill-customer-status").html(data.customer.status_member == 1 ? "Anggota" : "Non Anggota");

	var totalSaldo = data.customerAccounts.map(function (acct) {
		return acct.balance;
	}).reduce(function (total, item) {
		return total + item;
	}, 0);
	sectionDetailCustomer.find("span#fill-customer-totalSaving").html(data.customerAccounts.length);

	sectionDetailCustomer.find("span#fill-customer-totalBalance").html(totalSaldo);
	sectionDetailCustomer.find("span#fill-customer-totalLoan").html();

	drawDataTableSaving(data.customerAccounts, data.memberTransaction);
}

function drawDataTableSaving(data, dataTransaction) {
	var dateNow = new Date();
	var dateYearOnly = new Date(dateNow.getFullYear() + "-01-01 00:00:00");
	var dateMonthOnly = new Date(dateNow.getFullYear() + "-" + (dateNow.getMonth() + 1).toString().padStart(2, '0') + "-01 00:00:00");
	var _data = data.map(function (r) {
		var balance = 0;
		var sisaSaldo = 0;

		if (r["savings_products_id"] == 1) {
			balance = dataTransaction.filter(function (rowTran) {
				return rowTran["tran_type"] == "in";
			}).map(function (rowTran) {
				var detailFromBalance = rowTran.detail.filter(function (rowDetail) {
					return rowDetail["savings_products_id"] == r["savings_products_id"];
				}).map(function (rowDetail) {
					return rowDetail.amount;
				}).reduce(function (total, item) {
					return total + item;
				}, 0);

				return detailFromBalance;
			}).reduce(function (total, item) {
				return total + item;
			}, 0);
			sisaSaldo = r["saving_product"]["initial_deposit"] - balance;
		} else if (r["savings_products_id"] == 2) {
			balance = dataTransaction.filter(function (rowTran) {
				var arrayDateCreated = rowTran.created.split(" ");
				arrayDateCreated = arrayDateCreated[0].split("-");
				var dateCreated = new Date(arrayDateCreated[0] + "-" + arrayDateCreated[1] + "-01 00:00:00");
				return rowTran["tran_type"] == "in" && (dateCreated.getTime() == dateMonthOnly.getTime());
			}).map(function (rowTran) {
				var detailFromBalance = rowTran.detail.filter(function (rowDetail) {
					return rowDetail["savings_products_id"] == r["savings_products_id"];
				}).map(function (rowDetail) {
					return rowDetail.amount;
				}).reduce(function (total, item) {
					return total + item;
				}, 0);

				return detailFromBalance;
			}).reduce(function (total, item) {
				return total + item;
			}, 0);
			var otherBalance = dataTransaction.filter(function (rowTran) {
				var arrayDateCreated = rowTran.created.split(" ");
				arrayDateCreated = arrayDateCreated[0].split("-");
				var dateCreated = new Date(arrayDateCreated[0] + "-01-01 00:00:00");
				return rowTran["tran_type"] == "in" && (dateCreated.getTime() == dateYearOnly.getTime());
			}).map(function (rowTran) {
				var detailFromBalance = rowTran.detail.filter(function (rowDetail) {
					return rowDetail["savings_products_id"] == r["savings_products_id"];
				}).map(function (rowDetail) {
					return rowDetail.amount;
				}).reduce(function (total, item) {
					return total + item;
				}, 0);

				return detailFromBalance;
			}).reduce(function (total, item) {
				return total + item;
			}, 0);
			sisaSaldo = (r["saving_product"]["initial_deposit"] * 12) - otherBalance;
		} else {
			balance = dataTransaction.filter(function (rowTran) {
				return rowTran["tran_type"] == "in";
			}).map(function (rowTran) {
				var detailFromBalance = rowTran.detail.filter(function (rowDetail) {
					return rowDetail["savings_products_id"] == r["savings_products_id"];
				}).map(function (rowDetail) {
					return rowDetail.amount;
				}).reduce(function (total, item) {
					return total + item;
				}, 0);

				return detailFromBalance;
			}).reduce(function (total, item) {
				return total + item;
			}, 0);
			balance -= dataTransaction.filter(function (rowTran) {
				return rowTran["tran_type"] == "out";
			}).map(function (rowTran) {
				var detailFromBalance = rowTran.detail.filter(function (rowDetail) {
					return rowDetail["savings_products_id"] == r["savings_products_id"];
				}).map(function (rowDetail) {
					return rowDetail.amount;
				}).reduce(function (total, item) {
					return total + item;
				}, 0);

				return detailFromBalance;
			}).reduce(function (total, item) {
				return total + item;
			}, 0);
			if (r["saving_product"]["initial_deposit"] != 0) {
				sisaSaldo = r["saving_product"]["initial_deposit"] - balance;
			}
		}

		var newRow = $.extend(r, {
			simpananId: r["savings_products_id"],
			namaSimpanan: r["saving_product"]["name"],
			saldo: balance,
			depositAwal: r["saving_product"]["initial_deposit"],
			sisaSaldo: sisaSaldo,
		});

		return newRow;
	});

	drawDataTable(dataTableSaving, _data);


	// drawDataTable(dataTableTransaction, _data);
}

function renderSelectSavingIn() {
	var data = getAllDataTable(dataTableSaving);
	var _selectSaving = modalTransaction.find("#select-type-saving");
	_selectSaving.find("option:not(:first)").remove();

	savingProduct.forEach(function (r) {
		var findSavingCustomer = data.find(function (r2) {
			if (r2["savings_products_id"] == r.id && (r2["saldo"] == 0 || r2["depositAwal"] == 0 || r2["saldo"] < r2["depositAwal"])) {
				return r2;
			}
		});
		if (findSavingCustomer != undefined) {
			var option = $("<option></option>");
			option.val(r.id);
			option.html(r.name);
			_selectSaving.append(option);
		}
	});
	modalTransaction.find("#select-type-saving").trigger("change");
}

function renderSelectSavingOut() {
	var data = getAllDataTable(dataTableSaving);
	var _selectSaving = modalTransaction.find("#select-type-saving");
	_selectSaving.find("option:not(:first)").remove();

	savingProduct.forEach(function (r) {
		if (r.id == 1 || r.id == 2) return;
		var findSavingCustomer = data.find(function (r2) {
			if (r2["savings_products_id"] == r.id && r2["saldo"] > r2["depositAwal"]) {
				return r2;
			}
		});
		if (findSavingCustomer != undefined) {
			var option = $("<option></option>");
			option.val(r.id);
			option.html(r.name);
			_selectSaving.append(option);
		}
	});
	modalTransaction.find("#select-type-saving").trigger("change");
}

function renderSelectSavingJoin() {
	var data = getAllDataTable(dataTableSaving);
	var _selectSaving = modalJoinSaving.find("#select-new-type-saving");
	_selectSaving.find("option:not(:first)").remove();

	savingProduct.forEach(function (r) {
		var findSavingCustomer = data.find(function (r2) {
			if (r2["savings_products_id"] == r.id) {
				return r2;
			}
		});
		if(findSavingCustomer == undefined) {
			var option = $("<option></option>");
			option.val(r.id);
			option.html(r.name);
			_selectSaving.append(option);
		}
	});
	modalJoinSaving.find("#select-new-type-saving").trigger("change");
}

$("#btnTransactionIn").on("click", function (ev) {
	ev.preventDefault();

	modalTransaction.attr("data-tranType", "in");
	modalTransaction.find(".title-type-transaction").html("Masuk");
	renderSelectSavingIn();
	modalTransaction.modal({show: true, backdrop: 'static', keyboard: false});
});

$("#btnTransactionOut").on("click", function (ev) {
	ev.preventDefault();

	modalTransaction.attr("data-tranType", "out");
	modalTransaction.find(".title-type-transaction").html("Keluar");
	renderSelectSavingOut();
	modalTransaction.modal({show: true, backdrop: 'static', keyboard: false});
});

$("#btnJoinSaving").on("click", function (ev) {
	ev.preventDefault();

	renderSelectSavingJoin();
	modalJoinSaving.modal({show: true, backdrop: 'static', keyboard: false});
});

modalTransaction.on("shown.bs.modal", function () {
	dataTableTransaction.columns.adjust().draw();
});
modalTransaction.on("show.bs.modal", function () {
	drawDataTable(dataTableTransaction, []);
});

modalJoinSaving.find("button#btnYes").on("click", function (ev) {
	var member_id = sectionDetailCustomer.find("span#fill-customer-nik").html().trim();
	var select_saving_product = modalJoinSaving.find("#select-new-type-saving").val();

	if (StringUtils.isNotBlank(select_saving_product)) {
		API.customer.transaction.newSaving({
			member_id: member_id,
			saving_product: select_saving_product
		}, function (result) {
			API.customer.search({nik: member_id}, function (resultCustomerSearch) {
				if (resultCustomerSearch.code == 0) {
					localStorage.setItem("data-customer", JSON.stringify(resultCustomerSearch.data));
					modalJoinSaving.modal("hide");
					showDetail();
				}
			});
		}, null, true);
	}
});
modalTransaction.find("button#btnYes").on("click", function (ev) {
	ev.preventDefault();

	var data = getAllDataTable(dataTableTransaction);
	var nik = sectionDetailCustomer.find("span#fill-customer-nik").html().trim();

	if (modalTransaction.attr("data-tranType") == "in") {
		API.customer.transaction.in({transaction: data}, function (result) {
			API.customer.search({nik: nik}, function (resultCustomerSearch) {
				if (resultCustomerSearch.code == 0) {
					localStorage.setItem("data-customer", JSON.stringify(resultCustomerSearch.data));
					modalTransaction.modal("hide");
					showDetail();
				}
			});
		}, null, true);
	} else if (modalTransaction.attr("data-tranType") == "out") {
		API.customer.transaction.out({transaction: data}, function (result) {
			API.customer.search({nik: nik}, function (resultCustomerSearch) {
				if (resultCustomerSearch.code == 0) {
					localStorage.setItem("data-customer", JSON.stringify(resultCustomerSearch.data));
					modalTransaction.modal("hide");
					showDetail();
				}
			});
		}, null, true);
	}
});

modalJoinSaving.find("button#btnNo").on("click", function (ev) {
	ev.preventDefault();
	modalJoinSaving.modal("hide");
});

modalTransaction.find("button#btnNo").on("click", function (ev) {
	ev.preventDefault();
	modalTransaction.modal("hide");
});

modalTransaction.find("button#btnAddTran").on("click", function (ev) {
	ev.preventDefault();
	var savingType = modalTransaction.find("#select-type-saving").val();
	var nominal = modalTransaction.find(":input#input-nominal").val();

	var dataToPay = {
		amountToPay: removeCurrencyFormat(nominal),
		savingToPay: savingType,
	};

	console.log(dataToPay);

	if (StringUtils.isBlank(dataToPay.savingToPay) || parseFloat(dataToPay.amountToPay) == 0) {
		toastr.error("Jenis Simpanan & Nominal Uang wajib di isi.", "Error", {
			positionClass: 'md-toast-top-center',
			preventDuplicates: true,
		});
		return;
	}

	var dataCurrentSaving = getAllDataTable(dataTableSaving);
	var currentSaving = dataCurrentSaving.find(function (r) {
		return r["savings_products_id"] == savingType;
	});

	currentSaving = $.extend(currentSaving, dataToPay);

	var dataCurrentTransaction = getAllDataTable(dataTableTransaction);
	var iFound = -1;
	var currentTransaction = dataCurrentTransaction.find(function (r, i) {
		iFound = i;
		return r["savings_products_id"] == savingType;
	});

	if (currentTransaction == undefined && iFound == -1) {
		dataTableTransaction.row.add(currentSaving).draw();
	} else {
		dataTableTransaction.row(iFound).data(currentSaving).draw();
	}

	modalTransaction.find("#select-type-saving").val("").trigger("change");
	modalTransaction.find(":input#input-nominal").val("0.00").trigger("change");

	// console.log(savingType, removeCurrencyFormat(nominal));
});
modalJoinSaving.find("#select-new-type-saving").on("change", function () {
	var me = $(this);
	var meValue = me.val();

	modalJoinSaving.find("p.label-description-saving").empty();
	if (StringUtils.isNotBlank(meValue)) {
		var sp = savingProduct.find(function (r) {
			return r.id == meValue;
		});
		modalJoinSaving.find("p.label-description-saving").html(sp.description);
	}
});
modalTransaction.find("#select-type-saving").on("change", function () {
	var minNominal = null;
	var maxNominal = null;
	var me = $(this);
	var meValue = me.val();

	modalTransaction.find(":input#input-nominal").prop("readonly", false);
	modalTransaction.find("span.min-value-label").html(currencyFormat(0));
	modalTransaction.find("span.max-value-label").html(currencyFormat(0));

	if (StringUtils.isNotBlank(meValue)) {
		var dataCurrentSaving = getAllDataTable(dataTableSaving);

		var currentSaving = dataCurrentSaving.find(function (r) {
			return r["savings_products_id"] == meValue;
		});
		if (meValue == 1) {
			minNominal = 0;
			maxNominal = currentSaving["sisaSaldo"];
		} else if (meValue == 2) {
			minNominal = currentSaving["depositAwal"];
			maxNominal = currentSaving["depositAwal"];

			modalTransaction.find(":input#input-nominal").prop("readonly", true);
		} else {
			minNominal = 0;
			if(modalTransaction.attr("data-tranType") == "out") {
				maxNominal = currentSaving["saldo"];
			}
		}
	}

	var optMasking = {
		alias: 'rp',
	};

	if (minNominal != null) {
		optMasking["min"] = minNominal;
		modalTransaction.find("span.min-value-label").html(currencyFormat(minNominal));
	}
	if (maxNominal != null) {
		optMasking["max"] = maxNominal;
		modalTransaction.find("span.max-value-label").html(currencyFormat(maxNominal));
	}


	modalTransaction.find(":input#input-nominal").inputmask(optMasking).val(0.0).trigger("change");
});

var hash = window.location.hash;

if (StringUtils.isNotBlank(hash) && hash == "#detail") {
	showDetailWithoutAnimation();
} else {
	localStorage.removeItem("data-customer");
}
