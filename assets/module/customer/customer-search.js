$("form#formSearchCustomer button#btnSearchCustomer").on("click", function (ev) {
	ev.preventDefault();

	var nikSearch = $("form#formSearchCustomer input#input-nik-search");
	var nikSearchValue = nikSearch.val();

	if(StringUtils.isBlank(nikSearchValue)) {
		toastr.error(langTemplateSearchCustomer["nik-empty"]);
		return;
	}

	if(nikSearchValue.length < nikSearch.attr("minlength")) {
		toastr.error(langTemplateSearchCustomer["nik-lessthan-minlength"].replace("%s", nikSearch.attr("minlength")));
		return;
	}

	var dataSend = {
		nik : nikSearchValue
	};

	API.customer.search(dataSend, function (result) {
		if(typeof modalSearchCallback != "undefined") {
			modalSearchCallback(result, langTemplateSearchCustomer);
		}else if(typeof modalSearchSuccess != "undefined" && typeof modalSearchFailed != "undefined") {
			if(result.code == 0) {
				if(typeof modalSearchSuccess != "undefined") {
					modalSearchSuccess(result, langTemplateSearchCustomer);
				}else{
					toastr.success(result.message);
				}
			}else{
				if(typeof modalSearchFailed != "undefined") {
					modalSearchFailed(result, langTemplateSearchCustomer);
				}else{
					toastr.error(result.message);
				}
			}
		}
	}, null, true);
});

$("form#formSearchCustomer input#input-nik-search").on("keypress", function (ev) {
	if(ev.keyCode == 13) {
		ev.preventDefault();
		$("form#formSearchCustomer button#btnSearchCustomer").trigger("click");
		return false;
	}
});
