function getDataForm() {
	var form = $("form#form-login");
	var data = {};
	form.find("input").each(function () {
		if (StringUtils.isNotBlank($(this).val()))
			data[$(this).attr("id")] = $(this).val();
	});
	return data;
}

function onSuccessLogin(result) {
	if (result.code == "0") {
		window.location.reload();
	} else {
		$.notify({
			// options
			icon: "fa fa-exclamation-circle",
			title: "Failed to login",
			message: "Error Code : ["+result.code+"]<br>" + result.message,
			target: "_blank"
		}, {
			// settings
			element: "body",
			position: null,
			type: "danger",
			allow_dismiss: true,
			newest_on_top: true,
			placement: {
				from: "top",
				align: "center"
			},
			offset: {
				y: 20
			},
			spacing: 10,
			z_index: 1031,
			delay: 10000000,
			timer: 1000,
			url_target: "_blank",
			mouse_over: null,
			animate: {
				enter: "animated fadeInDown",
				exit: "animated fadeOutUp"
			},
			onShow: null,
			onShown: null,
			onClose: null,
			onClosed: null,
			icon_type: "class",
			template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
				'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
				'<div><span data-notify="icon"></span> ' +
				'<span data-notify="title">{1}</span></div>' +
				'<span data-notify="message">{2}</span>' +
				'<div class="progress" data-notify="progressbar">' +
				'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
				"</div>" +
				'<a href="{3}" target="{4}" data-notify="url"></a>' +
				"</div>"
		});
	}
}

var validator = $("form#form-login").validate({
	rules: {
		email: {
			required: true,
			email: true
		},
		password: {
			required: true,
			minlength: 5
		}
	},
	messages: {
		email: "Valid email is required: ex@abc.xyz",
		password: {
			required: "Please provide a password",
			minlength: "Your password must be at least 5 characters long"
		}
	},
	errorElement: "span",
	errorPlacement: function (error, element) {
		$(element)
			.removeClass("validate-success")
			.addClass("validate-error");
		$(element)
			.parent()
			.attr("data-validate", $(error).html());
	},
	success: function (label, element) {
		$(element)
			.removeClass("validate-error")
			.addClass("validate-success");
		$(element)
			.parent()
			.removeAttr("data-validate");
	},
	highlight: function (element, errorClass, validClass) {
		$(element)
			.parent()
			.addClass("alert-validate");
	},
	unhighlight: function (element, errorClass, validClass) {
		$(element)
			.parent()
			.removeClass("alert-validate");
	}
});

$("form#form-login :input").on("keypress", function (ev) {
	if (ev.keyCode == 13) {
		ev.preventDefault();
		$("button#btnLogin").trigger("click");
		return false;
	}
});

$("button#btnLogin").on("click", function (ev) {
	ev.preventDefault();

	if (!validator.form()) return false;

	var data = getDataForm();

	API.auth.login(data, onSuccessLogin, null, true);

});



API.synchronize.master.preload(
	null,
	function (result) {
		if (result.code == 0) {
			// API.auth.login(data, onSuccessLogin, null, true);
			loopNotificationSynchronize(result.data, function () {
				toastr.success(languagePopup["synchronize"]["success"]["message"]);
			});
		} else if(result.code == -3001 || result.code == -3002) {
			toastr.error(result.message);
		} else if(result.code == -3003) {
			toastr.info(result.message);
		} else {
			toastr.error(languagePopup["synchronize"]["failed"]["message"]);
		}
	},
	null,
	true
);
