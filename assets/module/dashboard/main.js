if(isOnline) {
	API.synchronize.customer.preload(
		null,
		function (result) {
			if (result.code == 0) {
				loopNotificationSynchronize(result.data, function () {
					toastr.success(languagePopup["synchronize"]["success"]["message"]);
				});
			} else if(result.code == -3001 || result.code == -3002) {
				toastr.error(result.message);
			} else if(result.code == -3003) {
				toastr.info(result.message);
			} else {
				toastr.error(languagePopup["synchronize"]["failed"]["message"]);
			}
		},
		null,
		true
	);
}

localStorage.removeItem("data-nik");
localStorage.removeItem("data-customer");

function modalSearchCallback(result, lang) {
	var modal = $("#modalSearchCustomer");

	if(result.code == 0) {
		modal.find(".modal-dialog").removeClass("modal-danger").addClass("modal-success");
		modal.find(".modal-title").html(lang["popupTitle-nik-already"]);
		modal.find(".modal-icon").removeClass("fa-times").addClass("fa-check");
		modal.find("#modal-message").html(lang["popupMessage-nik-already"]);
	}else{
		modal.find(".modal-dialog").addClass("modal-danger").removeClass("modal-success");
		modal.find(".modal-title").html(lang["popupTitle-nik-not-found"]);
		modal.find(".modal-icon").addClass("fa-times").removeClass("fa-check");
		modal.find("#modal-message").html(lang["popupMessage-nik-not-found"]);
	}

	modal.find("button#btnConfirmYes").on("click", function (ev) {
		ev.preventDefault();

		if(result.code == 0) {
			localStorage.setItem("data-customer", JSON.stringify(result.data));

			window.location.href = site_url + "pages/customer/search#detail";
		}else{
			localStorage.setItem("data-nik", $("form#formSearchCustomer input#input-nik-search").val());

			window.location.href  = site_url + "pages/customer/registration#withnik";
		}
	});

	modal.modal("show");
}

/*
function modalSearchSuccess(result, lang) {
	var modal = $("#modalSearchCustomer");

	modal.find(".modal-dialog").removeClass("modal-danger").addClass("modal-success");
	modal.find(".modal-title").html(lang);
	modal.find(".modal-icon").removeClass("fa-times").addClass("fa-check");
	modal.find("#modal-message").html("Berhasil menemukan anggota<br>Apakah anda ingin melanjutkan untuk simpanan / pinjaman ?");

	modal.find("button#btnConfirmYes").on("click", function (ev) {
		ev.preventDefault();

		localStorage.setItem("data-customer", JSON.stringify(result.data));

		window.location.href = site_url + "pages/customer/search#detailwithnik";
	});

	modal.modal("show");
}

function modalSearchFailed(result, lang) {
	var modal = $("#modalSearchCustomer");

	modal.find(".modal-dialog").addClass("modal-danger").removeClass("modal-success");
	modal.find(".modal-title").html("Gagal menemukan anggota");
	modal.find(".modal-icon").addClass("fa-times").removeClass("fa-check");
	modal.find("#modal-message").html("Tidak menemukan data anggota<br>Apakah anda ingin melanjutkan untuk pendaftaran ?");

	modal.find("button#btnConfirmYes").on("click", function (ev) {
		ev.preventDefault();

		localStorage.setItem("data-nik", $("form#formSearchCustomer input#input-nik-search").val());

		window.location.href  = site_url + "pages/customer/registration#withnik";
	});

	modal.modal("show");
}
*/
