// Button Side Nav Initialization
$(".button-collapse").sideNav();

// event click has link
$("[data-redirect]").on("click", function (ev) {
	ev.preventDefault();
	var urlRedirect = $(this).attr("data-redirect");
	if (urlRedirect.indexOf(site_url) == -1) {
		urlRedirect = site_url + urlRedirect;
	}
	if (StringUtils.isNotBlank(urlRedirect)) window.location.href = urlRedirect;
});

// Input Search Menu Initialization
var timeoutInput = null;
var eventKeypress = jQuery.Event("keypress");
var eventKeyup = jQuery.Event("keyup");
eventKeypress.keyCode = 13;
$(".search-menu").on("keyup", function (ev) {
	if (timeoutInput != null) {
		clearTimeout(timeoutInput);
		timeoutInput = null;
	}
	var valueSearch = $(this).val().trim().toLowerCase();
	timeoutInput = setTimeout(function () {
		var sidebarContainer = $(".sidebar-menu");
		var menuItems = sidebarContainer.find("li span.can-search");
		sidebarContainer
			.find("ul, li")
			.removeClass("d-none")
			.removeClass("do-search");
		menuItems.each(function () {
			var menuItem = $(this);
			var menuTitle = menuItem.text().toLowerCase();
			var regex = new RegExp(valueSearch, "g");
			if (menuTitle.match(regex)) {
				menuItem
					.parents("ul.sidebar-menu ul, ul.sidebar-menu li")
					.addClass("do-search");
			}
		});
		menuItems.each(function () {
			var menuItem = $(this);
			var parent = menuItem.parents("ul.sidebar-menu ul, ul.sidebar-menu li");
			parent.each(function () {
				var p = $(this);
				if (p.hasClass("menu-item-dropdown")) {
					var uc = p.children(".collapsible-body").children("ul");
					if (!uc.hasClass("do-search")) {
						p.removeClass("do-search");
						p.addClass("d-none");
					}
				} else if (!p.hasClass("do-search")) {
					p.addClass("d-none");
				}
			});
		});
		sidebarContainer = $(".sidebar-menu");

		var title = sidebarContainer.find(".title-header");
		var iHeader = [];
		title.each(function () {
			var i = sidebarContainer
				.children("li")
				// console.log(i);
				.index($(this).parent());
			iHeader.push(i);
		});

		var ikk = [];
		for (var i = 0; i < iHeader.length; i++) {
			var elCurrent = iHeader[i];
			var elNext = iHeader[i + 1];
			var mis = sidebarContainer.children("li");
			ikk.push([]);
			if (elNext != undefined) {
				for (var j = 0; j < mis.length; j++) {
					if (elCurrent != j) {
						if (j < elNext) {
							ikk[ikk.length - 1].push(j);
						}
					}
				}
			} else {
				for (var m = elCurrent + 1; m < mis.length; m++) {
					ikk[ikk.length - 1].push(m);
				}
			}
		}
		for (var i = 0; i < iHeader.length; i++) {
			var ik = "li:eq(" + ikk[i].join("), li:eq(") + ")";
			var q = sidebarContainer.children(ik).filter(".do-search");

			if (q.length == 0) {
				sidebarContainer
					.children("li:eq(" + iHeader[i] + ")")
					.addClass("d-none");
			}
		}
		title = sidebarContainer.children("li:not(.d-none):not(.do-search)");
		if (title.length == 0) {
			$(".search-menu").val("").trigger(eventKeyup);
		}
	}, 100);
});
$(".search-menu").trigger(eventKeyup);

// Button Logout Initialization
$("#btnLogout").on("click", function (ev) {
	ev.preventDefault();

	API.auth.logout(
		null,
		function (result) {
			if (result.code == 0) {
				window.location.reload();
			} else {
				popupModalApi.failed(languagePopup["logout"], result.message);
			}
		},
		null
	);
});

var loopNotificationInterval = null;

function loopNotificationSynchronize(data, cb, index) {
	if (index == undefined) index = 0;

	var keys_data = Object.keys(data);

	var key_data = keys_data[index];
	var item_data = data[key_data];

	if(typeof item_data == "object") {
		if (item_data["status"] == 1) {
			toastr.success(languagePopup["synchronize"]["success"]["message"] + " " + item_data["length_data"]["success"] + " " + key_data.replace(/\_/g, " ").ucwords());
		} else if (item_data["status"] == 2) {
			var textPopup = "";
			textPopup += languagePopup["synchronize"]["success"]["message"] + " " + item_data["length_data"]["success"] + " " + key_data.replace(/\_/g, " ").ucwords();
			textPopup += "<br>";
			textPopup += languagePopup["synchronize"]["failed"]["message"] + " " + item_data["length_data"]["failed"] + key_data.replace(/\_/g, " ").ucwords();
			toastr.warning(textPopup);
		} else if (item_data["status"] == 3) {
			toastr.info(key_data.replace(/\_/g, " ").ucwords() + " " + languagePopup["synchronize"]["nothing_to_sync"]["message"]);
		} else {
			toastr.error(languagePopup["synchronize"]["failed"]["message"] + " " + key_data.replace(/\_/g, " ").ucwords());
		}
	}else if (typeof item_data == "boolean") {
		if (item_data == true) {
			toastr.success(languagePopup["synchronize"]["success"]["message"] + " " + key_data.replace(/\_/g, " ").ucwords());
		} else {
			toastr.error(languagePopup["synchronize"]["failed"]["message"] + " " + key_data.replace(/\_/g, " ").ucwords());
		}
	}

	setTimeout(function () {
		++index;
		if (index < keys_data.length) {
			loopNotificationSynchronize(data, cb, index);
		} else {
			setTimeout(function () {
				if (cb == undefined) {
					window.location.reload();
				} else {
					cb();
				}
			}, 1000);
			// loopNotificationInterval = setInterval(function () {
			// 	var toastContainer = $("#toast-container");
			// 	if (toastContainer.length == 0) {
			// 		if (cb == undefined) {
			// 			window.location.reload();
			// 		} else {
			// 			cb();
			// 		}
			// 		clearInterval(loopNotificationInterval);
			// 	}
			// }, 500);
		}
	}, 1000);
}

// Button Synchronize Initialization
$("#btnSync").on("click", function (ev) {
	ev.preventDefault();

	API.synchronize.load(
		null,
		function (result) {
			if (result.code == 0) {
				loopNotificationSynchronize(result.data);
			} else if (result.code == -3001 || result.code == -3002) {
				toastr.error(result.message);
			} else if(result.code == -3003) {
				toastr.info(result.message);
			} else {
				toastr.error(languagePopup["synchronize"]["failed"]["message"]);
			}
		},
		null,
		true
	);
});

// Button Synchronize Initialization
$("#btnToCustomerRegister").on("click", function (ev) {
	ev.preventDefault();

	$("header a[data-redirect*='customer/registration']").trigger("click");
});

// Tilt Initialization
$(".js-tilt").tilt({
	scale: 1.1
});

// Custom Scrollbar Initialization
$(".custom-scrollbar").each(function () {
	var psi = new PerfectScrollbar(this, {
		wheelSpeed: 1,
		wheelPropagation: true,
		minScrollbarLength: 20
	});
	$.extend(this, {ps: psi});
});

// event handler window resize
$(window).on("resize", function () {
	$(".custom-scrollbar").each(function () {
		this.ps.update();
	});
});

// Select2 Initialization
$.fn.select2.defaults.set("theme", "material");
$.fn.select2.defaults.set("ignore", "input[type=hidden], .select2-input, .select2-focusser");
$(".select2").each(function () {
	if ($(this).attr("placeholder")) {
		var optionBlank = $("<option></option>");
		optionBlank.val("");
		optionBlank.html($(this).attr("placeholder"));
		$(this).prepend(optionBlank);
	}
	$(this).select2({
		allowClear: $(this).attr("placeholder") != undefined && $(this).attr("placeholder") != null,
		placeholder: $(this).attr("placeholder"),
		minimumResultsForSearch: $(this).attr("no-search") ? -1 : Infinity,
	});
	$(this).val("").trigger("change");
	$(this).on('change', function () {
		$(this).trigger('blur');
	});
});

// Tooltip Initialization
$('[data-toggle="tooltip"]').each(function () {
	$(this).tooltip();
});

// Add New Method for Validation Phone ID
$.validator.addMethod("phoneID".toLowerCase(), function (value, element) {
	return this.optional(element) || /^\d{3}-\d{4}-\d{4}$/.test(value);
}, languageForm["phoneID"]);

$.validator.addMethod("nikNotRegistered".toLowerCase(), function (value, element) {
	var response_data = false;
	JSON.post_async("customer/search", {
		nik: value
	}, function (result) {
		if(result.code != "0") response_data = true;
	}, null, 100000, false);
	return response_data;
}, languageForm["nikNotRegistered"]);

// Set Default Validator
$.validator.setDefaults({
	// debug: true,
	errorElement: "span",
	errorPlacement: function (error, element) {
		error.addClass("red-text");
		element.addClass("invalid");
		if (element.prop("type") === "checkbox") {
			error.insertAfter(element.parent("label"));
		} else {
			element.parent().append(error);
		}
	},
	success: function (label, element) {
		$(element).removeClass("invalid");
		if ($(element).parent().find("span.error")) {
			$(element).parent().find("span.error").remove();
		}
	},
	highlight: function (element, errorClass, validClass) {
		$(element).addClass("invalid");
	},
	unhighlight: function (element, errorClass, validClass) {
		$(element).removeClass("invalid");
		if ($(element).parent().find("span.error")) {
			$(element).parent().find("span.error").remove();
		}
	},
});

// event handler language
$("#navbarLanguage").next().children("a").on("click", function (ev) {
	ev.preventDefault();

	var $code = $(this).data("country-code");

	if (StringUtils.isNotBlank($code)) {
		API.changeLanguage({code: $code}, function (result) {
			window.location.reload();
		}, null, true);
	}
});

// Initialization datepicker
$(".datepicker").each(function () {
	var opt = languageForm["datepicker"];
	if ($(this).attr("data-max")) {
		opt["max"] = JSON.parse($(this).attr("data-max"));
		opt["max"][1] = opt["max"][1] - 1;
	}
	if ($(this).attr("data-min")) {
		opt["min"] = JSON.parse($(this).attr("data-min"));
	}
	if ($(this).attr("data-format")) {
		opt["format"] = $(this).attr("data-format");
	}
	if ($(this).attr("data-formatSubmit")) {
		opt["formatSubmit"] = $(this).attr("data-formatSubmit");
	} else {
		$(this).attr("data-formatSubmit", opt["formatSubmit"]);
	}

	$(this).pickadate(opt);
});

Inputmask.extendAliases({
	rp: {
		prefix: "Rp. ",
		groupSeparator: ".",
		alias: "currency",
		placeholder: "0",
		allowMinus: false,
		autoGroup: !0,
		digits: 2,
		digitsOptional: !1,
		clearMaskOnLostFocus: !1,
		radixPoint : ",",
		showMaskOnHover: false,
		showMaskOnFocus: false,
		rightAlign: false,
	}
});

$(".inputmask").each(function () {
	var optMask = {
		showMaskOnHover: false,
		showMaskOnFocus: false,
	};
	$(this).inputmask(optMask);
});

$(document).ready(function () {
	$(".fixed-action-btn-main").each(function () {
		var defaultHeight = $(this).find("ul").height();

		$(this).on("mouseenter", function () {
			var $this = $(this);
			$.fn.openFAB($this);

			$this.find("ul").css({
				"height": defaultHeight * $this.find("ul li").length + "px"
			});
		});
		$(this).on("mouseleave", function () {
			var $this = $(this);
			$.fn.closeFAB($this);

			$this.find("ul").css({
				"height": defaultHeight + "px"
			});
		});
		$(this).children("a").on("click", function () {
			var $this = $(this);
			var $menu = $this.parent();
			if ($menu.hasClass('active')) {
				$.fn.openFAB($menu)

				$this.find("ul").css({
					"height": defaultHeight * $this.find("ul li").length + "px"
				});
			} else {
				$.fn.closeFAB($menu);

				$this.find("ul").css({
					"height": defaultHeight + "px"
				});
			}

			if ($menu.hasClass('active')) {
				$.fn.closeFAB($menu);

				$this.find("ul").css({
					"height": defaultHeight + "px"
				});
			} else {
				$.fn.openFAB($menu);

				$this.find("ul").css({
					"height": defaultHeight * $this.find("ul li").length + "px"
				});
			}
		});
	});
});

var popupModalApi = {
	success: function (title, message) {
		var modalApi = $(".modal#modalApi");
		modalApi.children(".modal-dialog").removeClass("modal-success modal-danger");
		modalApi.find(".modal-body #icon-modal-api").removeClass("fa-check fa-times");

		modalApi.children(".modal-dialog").addClass("modal-success");
		modalApi.find(".modal-body #icon-modal-api").addClass("fa-check");


		if (typeof title == "string") {
			modalApi.find(".modal-header #title-modal-api").html(title);
			modalApi.find(".modal-body #message-modal-api").html(message);
		} else if (typeof title == "object") {
			if (typeof title["success"] != "undefined" && typeof title["success"]["title"] != "undefined") {
				modalApi.find(".modal-header #title-modal-api").html(title["success"]["title"]);
			}
			if (StringUtils.isNotBlank(message)) {
				modalApi.find(".modal-body #message-modal-api").html(message);
			} else {
				if (typeof title["success"] != "undefined" && typeof title["success"]["message"] != "undefined") {
					modalApi.find(".modal-header #title-modal-api").html(title["success"]["message"]);
				}
			}
		}

		modalApi.modal({show: true, backdrop: 'static', keyboard: false});
	},
	failed: function (title, message) {
		var modalApi = $(".modal#modalApi");
		modalApi.children(".modal-dialog").removeClass("modal-success modal-danger");
		modalApi.find(".modal-body #icon-modal-api").removeClass("fa-check fa-times");

		modalApi.children(".modal-dialog").addClass("modal-danger");
		modalApi.find(".modal-body #icon-modal-api").addClass("fa-times");

		if (typeof title == "string") {
			modalApi.find(".modal-header #title-modal-api").html(title);
			modalApi.find(".modal-body #message-modal-api").html(message);
		} else if (typeof title == "object") {
			if (typeof title["failed"] != "undefined" && typeof title["failed"]["title"] != "undefined") {
				modalApi.find(".modal-header #title-modal-api").html(title["failed"]["title"]);
			}
			if (StringUtils.isNotBlank(message)) {
				modalApi.find(".modal-body #message-modal-api").html(message);
			} else {
				if (typeof title["failed"] != "undefined" && typeof title["failed"]["message"] != "undefined") {
					modalApi.find(".modal-header #title-modal-api").html(title["failed"]["message"]);
				}
			}
		}

		modalApi.modal({show: true, backdrop: 'static', keyboard: false});
	},
};

$("#btnCloseApp").on("click", function (ev) {
	ev.preventDefault();
	window.open('', '_parent', '');
	window.close();
});

$.extend($.fn.dataTable.defaults, {
	"language": {
		"decimal":        ",",
		"emptyTable":     "Tidak ada data.",
		"info":           "Menampilkan _START_ - _END_ dari _TOTAL_ data",
		"infoEmpty":      "Menampilkan 0 data",
		"infoFiltered":   "(pencarian dari _MAX_ data)",
		"infoPostFix":    "",
		"thousands":      ".",
		"lengthMenu":     "Tampil _MENU_ data",
		"loadingRecords": "Menunggu...",
		"processing":     "Sedan diproses...",
		"search":         "Cari:",
		"zeroRecords":    "Tidak ada pencarian yang cocok",
		"paginate": {
			"first":      "Awal",
			"last":       "Akhir",
			"next":       "Berikut",
			"previous":   "Sebelum"
		},
		"aria": {
			"sortAscending":  ": activate to sort column ascending",
			"sortDescending": ": activate to sort column descending"
		}
	}
});

function getAllDataTable(_DATA_TABLE) {
	var _d = _DATA_TABLE.rows().data();
	var _data = [];
	for (var i = 0; i<_d.length; i++) {
		_data.push(_d[i]);
	}
	return _data;
}

function getDataTableByRow(_DATA_TABLE, tr) {
	var _d = _DATA_TABLE.row(tr).data();
	var _data = [];
	for (var i = 0; i<_d.length; i++) {
		_data.push(_d[i]);
	}
	return _data;
}

function drawDataTable(_DATA_TABLE, _DATA) {
	_DATA_TABLE.clear();
	if(_DATA.length > 0) _DATA_TABLE.rows.add(_DATA);
	_DATA_TABLE.draw();
}

function currencyFormat(number) {
	if(typeof number !== "string") number = number.toString();
	return "Rp. " + parseFloat(number).toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,").replace(".", "-").replace(/\,/g,".").replace("-", ",");
}

function removeCurrencyFormat(currency) {
	if(typeof currency !== "string") currency = currency.toString();
	return currency.replace("Rp.", "").replace(/\./g, "").replace(",",".").trim();
}
