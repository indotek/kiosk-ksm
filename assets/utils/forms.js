$.fn.id = function () {
	return this.attr("id");
};
$.fn.name = function () {
	return this.attr("name");
};

$.fn.clear_key = function (pattern) {
	if(pattern == undefined || pattern == null) pattern = /^(input|select|textarea|cb|rb|form)\-/g;
	var id = this.id();
	if (StringUtils.isNotBlank(id)) return id.replace(pattern, "");
	else return "";
};


$.fn.form = function () {
	var Form = function () {

		this.clear = function () {
			this.form("set", {});
		};

		this.get_input = function ($input, data) {

			if (data == undefined || data == null) data = {};

			if (typeof $input == "string") {
				if (StringUtils.isBlank($input)) {
					throw SyntaxError("selector is empty");
					return;
				}
				$input = this.find($input);
			}
			if ($input.length == 0) {
				throw SyntaxError("element input undefined");
				return;
			}
			var key = $input.clear_key();
			if (StringUtils.isNotBlank(key)) {
				key = key.GENERATE_KEY("-", "");
				if ($input.is(":checkbox")) {
					data[key] = $input.is(":checked") ? "1" : "0";
				} else if ($input.is(":radio")) {
					var name = $input.name();
					var checkedRadio = $input.parents("form").find(":radio[name=" + name + "]:checked");
					if (checkedRadio.length > 0) data[key] = checkedRadio.val().toUpperCase();
				} else {
					if($input.hasClass("picker__input")) {
						var picker = $input.pickadate('picker');
						data[key] = picker.get('select', $input.attr("data-formatSubmit"));
					}else
					if (StringUtils.isNotBlank($input.val())) {
						data[key] = $input.val();
					}
				}
			}

			return data;
		};

		this.get = function () {
			var _this = this;
			var $input = this.find(":input");

			var data = this.data("input");
			if (data == undefined || data == null) data = {};

			$input.each(function () {
				var _this2 = $(this);
				data = _this.get_input(_this2, data);
			});

			return data;
		};

		this.set = function (data) {
			var _this = this;
			var $input = this.find(":input");
			if (data == undefined || data == null) data = {};

			var boolIsTrue = ["1", "Y", "TRUE"];
			var boolIsFalse = ["0", "N", "FALSE"];

			$input.each(function () {
				var _this2 = $(this);
				var key = _this2.clear_key();

				key = key.split("-");
				var data2 = $.extend(true, {}, data);

				if (!(_this2.is(":checkbox") || _this2.is(":radio"))) {
					key.forEach(function (k) {
						if (data2[k] != undefined) {
							if (typeof data2[k] == "object") {
								data2 = data2[k];
							} else if (
								typeof data2[k] == "string"
								|| typeof data2[k] == "number"
								|| typeof data2[k] == "bigint"
								|| typeof data2[k] == "boolean"
							) {
								_this2.val(data2[k].toString());
							}
						} else {
							_this2.val("").trigger("change");
						}
					});
					data2 = $.extend(true, {}, data);
				} else {
					key.forEach(function (k) {
						if (data2[k] != undefined) {
							if (typeof data2[k] == "object") {
								data2 = data2[k];
							} else if (
								typeof data2[k] == "string"
								|| typeof data2[k] == "number"
								|| typeof data2[k] == "bigint"
								|| typeof data2[k] == "boolean"
							) {
								var val = data2[k].toString().toUpperCase();
								if (_this2.is(":checkbox")) {
									if (boolIsTrue.indexOf(val) != -1) _this2.prop("checked", true);
									else _this2.prop("checked", false);
								} else if (_this2.is(":radio")) {
									if (_this2.val().toUpperCase() == val) _this2.prop("checked", true);
									else _this2.prop("checked", false);
								}
							}
						} else {
							_this2.val("").trigger("change");
						}
					});
					data2 = $.extend(true, {}, data);
				}

				_this2.trigger("change");
			});
		};

		var args = [];
		for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);

		return this[arguments[0]].apply(this, args.slice(1));
	};

	if (!this.is("form")) {
		throw SyntaxError("element is not form");
		return;
	}

	var args = [];
	for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);

	return Form.apply(this, args);
};
