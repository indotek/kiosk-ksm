if (typeof API !== "object") {
	var API = {};
}

function argumentsToArray(argument) {
	var args = [];
	for (var i = 0; i < argument.length; i++) args.push(argument[i]);
	return args;
}


function insertToIndex($array, $index, $data) {
	var $array1 = $array.slice(0, $index);
	$array1 = $array1.concat($data);
	var $array2 = $array.slice($index, $array.length);
	var $result = $array1.concat($array2);
	return $result;
}

API = {
	"changeLanguage": function (data, cbSuccess, cbFailed, isLoading) {
		JSON.post("changeLanguage", data, cbSuccess, cbFailed, 100000, isLoading);
	},
	"auth": {
		"login": function (data, cbSuccess, cbFailed, isLoading) {
			JSON.post("auth/login", data, cbSuccess, cbFailed, 100000, isLoading);
		},
		"logout": function (data, cbSuccess, cbFailed, isLoading) {
			JSON.post("auth/logout", data, cbSuccess, cbFailed, 100000, isLoading);
		},
	},
	"customer": {
		"registration": function (data, cbSuccess, cbFailed, isLoading) {
			JSON.post("customer/registration", data, cbSuccess, cbFailed, 100000, isLoading);
		},
		"search": function (data, cbSuccess, cbFailed, isLoading) {
			JSON.post("customer/search", data, cbSuccess, cbFailed, 100000, isLoading);
		},
		"transaction": {
			"in": function (data, cbSuccess, cbFailed, isLoading) {
				JSON.post("customer/transaction/in", data, cbSuccess, cbFailed, 100000, isLoading);
			},
			"out": function (data, cbSuccess, cbFailed, isLoading) {
				JSON.post("customer/transaction/out", data, cbSuccess, cbFailed, 100000, isLoading);
			},
			"newSaving": function (data, cbSuccess, cbFailed, isLoading) {
				JSON.post("customer/transaction/newSaving", data, cbSuccess, cbFailed, 100000, isLoading);
			},
		}
	},
	"master": {
		"regency": {
			"list": function (data, cbSuccess, cbFailed, isLoading) {
				JSON.post("master/regency/list", data, cbSuccess, cbFailed, 100000, isLoading);
			},
		},
		"district": {
			"list": function (data, cbSuccess, cbFailed, isLoading) {
				JSON.post("master/district/list", data, cbSuccess, cbFailed, 100000, isLoading);
			},
		},
		"village": {
			"list": function (data, cbSuccess, cbFailed, isLoading) {
				JSON.post("master/village/list", data, cbSuccess, cbFailed, 100000, isLoading);
			},
		},
	},
	"synchronize": {
		"load": function (data, cbSuccess, cbFailed, isLoading) {
			JSON.post("synchronize/load", data, cbSuccess, cbFailed, 100000, isLoading);
		},
		"customer": {
			"preload": function (data, cbSuccess, cbFailed, isLoading) {
				JSON.post("synchronize/customer/preload", data, cbSuccess, cbFailed, 100000, isLoading);
			},
		},
		"master": {
			"preload": function (data, cbSuccess, cbFailed, isLoading) {
				JSON.post("synchronize/master/preload", data, cbSuccess, cbFailed, 100000, isLoading);
			}
		},
	}
};
