var StringUtils = {
	isNotBlank: function (string) {
		return (string != undefined && StringUtils.isString(string) && string != null && string.trim().length > 0);
	},
	isBlank: function (string) {
		return !StringUtils.isNotBlank(string);
	},
	isString: function (string) {
		return (typeof string == 'string');
	},
	isNumber: function (string) {
		return (typeof string == 'number');
	},
	isEmail: function (string) {
		return string.match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) != null;
	}
};

String.prototype.GENERATE_KEY = function (splitKey, joinKey) {
	var target = this.toString();
	if(StringUtils.isBlank(target)) return target;
	target = target.split(splitKey);
	target = target.map(function (value, i) {
		if(i!=0) return value.substring(0,1).toUpperCase() + value.substring(1,value.length);
		else return value;
	});
	return target.join(joinKey);
};

String.prototype.ucwords = function() {
	var str = this.toLowerCase();
	return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
		function(s){
			return s.toUpperCase();
		});
};
