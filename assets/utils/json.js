if (typeof JSON !== "object") {
	var JSON = {};
}

// var showProgress = function() {
//     var _body = $("body");
//     var _divContainer = $("<div></div>");
//     _divContainer.addClass("containerLoading");

//     var _imgLoading = $("<i></i>");
//     _imgLoading.addClass("fa fa-spinner fa-10x fa-spin");

//     var _textLoading = $("<span></span>");
//     _textLoading.html("Loading");
//     _textLoading.addClass("textLoading");

//     var _containerLoading = $("<div></div>");
//     _containerLoading.addClass("imageLoading");

//     _containerLoading.append(_imgLoading, _textLoading);

//     _divContainer.append(_containerLoading);

//     _body.append(_divContainer);
//     return _divContainer;
// };
var $animateCallbackOne = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

var showProgress = function () {
	var _body = $("body");
	var _divContainer = $("<div></div>");
	_divContainer.addClass("containerLoading animated fadeIn");
	var _divLoading = $('<div class="preloader-wrapper big active" style="width:8rem;height:8rem;"><div class="spinner-layer spinner-blue-only"><div class="circle-clipper left"><div class="circle"></div></div><div class="gap-patch"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>');

	var _containerLoading = $("<div></div>");
	_containerLoading.addClass("imageLoading");
	_containerLoading.append(_divLoading);

	var _textLoading = $("<span></span>");
	_textLoading.html("Loading");
	_textLoading.addClass("textLoading");

	_divContainer.append(_containerLoading, _textLoading);
	_body.prepend(_divContainer);

	return _divContainer;
};

var hideProgress = function (_divContainer) {
	_divContainer.removeClass("fadeIn").addClass("fadeOut").one($animateCallbackOne, function () {
		_divContainer.remove();
	});
};

var ERROR_CODE = {
	timeout: {
		code: -1,
		message: "Request timeout"
	},
	error: {
		code: -2,
		message: "Problem with request"
	}
};

JSON["post"] = function (url, obj, success, fail, timeout, isLoading) {
	if (isLoading == undefined) isLoading = false;
	if (obj == null || typeof obj != "object" || obj == undefined) obj = {};

	var _divLoading;
	if (isLoading) _divLoading = showProgress();
	var _data = JSON.stringify(obj);

	url = api_url + url;
	
	console.info("URL_API=" + url);
	
	console.info("SENT_DATA=" + _data);

	$.ajax({
		type: "POST",
		url: url,
		data: _data,
		headers: { 'X-Language-Api': language },
		contentType: "application/json",
		dataType: "json",
		timeout: timeout
	})
		.done(function (data, status, xhr) {
			console.info("RESPONSE_DATA=" + JSON.stringify(data));
			try {
				if (null != success && typeof success == "function") {
					success(data, xhr);
				}
			} catch (e) {
				console.error(e);
			}
		})
		.fail(function (err) {
			console.info(JSON.stringify(err));
			try {
				var _ret = {};
				_ret["code"] = ERROR_CODE[err["statusText"]]["code"];
				_ret["message"] = ERROR_CODE[err["statusText"]]["message"];

				if (null != fail && typeof fail == "function") {
					fail(_ret);
				}
			} catch (e) {
				console.error(e);
			}
		})
		.always(function (resp) {
			if (isLoading) hideProgress(_divLoading);
		});
};

JSON["post_async"] = function (url, obj, success, fail, timeout, isLoading) {
	if (isLoading == undefined) isLoading = false;
	if (obj == null || typeof obj != "object" || obj == undefined) obj = {};

	var _divLoading;
	if (isLoading) _divLoading = showProgress();
	var _data = JSON.stringify(obj);

	url = api_url + url;

	console.info("URL_API=" + url);

	console.info("SENT_DATA=" + _data);

	$.ajax({
		type: "POST",
		url: url,
		data: _data,
		headers: { 'X-Language-Api': language },
		contentType: "application/json",
		dataType: "json",
		async: false,
		timeout: timeout
	})
		.done(function (data, status, xhr) {
			console.info("RESPONSE_DATA=" + JSON.stringify(data));
			try {
				if (null != success && typeof success == "function") {
					success(data, xhr);
				}
			} catch (e) {
				console.error(e);
			}
		})
		.fail(function (err) {
			console.info(JSON.stringify(err));
			try {
				var _ret = {};
				_ret["code"] = ERROR_CODE[err["statusText"]]["code"];
				_ret["message"] = ERROR_CODE[err["statusText"]]["message"];

				if (null != fail && typeof fail == "function") {
					fail(_ret);
				}
			} catch (e) {
				console.error(e);
			}
		})
		.always(function (resp) {
			if (isLoading) hideProgress(_divLoading);
		});
};
