<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Role_model extends CI_Model
{
	var $dbHosting;

	public function __construct()
	{
		parent::__construct();
	}

	public function sync($log_sync)
	{
		$dateNow = date('Y-m-d H:i:s');
		$this->dbHosting = connect_db_hosting();

		$this->db->trans_begin();

		$tableNameLocal = "role";
		$tableNameHosting = "aros";

		$x = $this->dbHosting->get($tableNameHosting);

		$iDataSync = 0;

		if ($x->num_rows() > 0) {
			$q = $x->result_array();
			foreach ($q as $key => $val) {
				$whereId = array("id" => $val["id"]);
				$this->db->where($whereId);
				$dataLocal = $this->db->get($tableNameLocal)->row_array();

				$valLocal = array(
					"id" => $val["id"],
					"name" => $val["alias"],
					"status" => $val["status"],
					"created" => $val["created"],
					"modified" => isset($val["modified"]) ? $val["modified"] : null,
				);

				if ($dataLocal != null) {
					$this->db->where($whereId);
					if ($val["status"] == 1) {
						$this->db->update($tableNameLocal, $valLocal);
					} else {
						$this->db->delete($tableNameLocal);
					}
				} else {
					$this->db->insert($tableNameLocal, $valLocal);
				}

				if (!($this->db->trans_status() === FALSE)) {
					$iDataSync++;
				}
			}

			$autoIncrementHosting = get_ai_hosting($tableNameHosting);

			if ($autoIncrementHosting != null && isset($autoIncrementHosting["AUTO_INCREMENT"])) {
				update_ai_local($tableNameLocal, $autoIncrementHosting["AUTO_INCREMENT"]);
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->dbHosting->close();
			log_sync_history_detail($tableNameLocal, 0, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 0,
				"length_data" => array("failed" => $iDataSync, "success" => 0),
			);
		} else {
			$this->db->trans_commit();
			$this->dbHosting->close();
			log_sync_history_detail($tableNameLocal, 1, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => ($iDataSync > 0) ? 1 : 3,
				"length_data" => array("success" => $iDataSync, "failed" => 0),
			);
		}
	}
}
