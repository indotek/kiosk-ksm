<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerTransactionDetail_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function sync($log_sync)
	{
		$tableName = "m_saving_transaction_detail";
		$dbHosting = connect_db_hosting();
		$dateNow = date('Y-m-d H:i:s');

		$dbHosting->trans_begin();

		$iDataSync = 0;
		
		$membersTransactionDetailLocal = $this->db->get($tableName)->result_array();

		foreach ($membersTransactionDetailLocal as $keyAcct => $valAcct) {
			$dbHosting->where("nik", $valAcct["member_id"]);
			$memberHosting = $dbHosting->get("m_members")->row_array();

			$dbHosting->where("member_id", $memberHosting["id"]);
			$dbHosting->where("created", $valAcct["created"]);
			$tranHosting = $dbHosting->get("m_saving_transaction")->row_array();

			$valAcctHost = $valAcct;

			unset($valAcctHost["member_id"]);

			$whereMemberTransactionDetailHosting = array(
				"tran_id" => $tranHosting["id"],
				"savings_products_id" => $valAcct["savings_products_id"],
				"created" => $valAcct["created"],
			);

			$valAcctHost["tran_id"] = $tranHosting["id"];

			$dbHosting->where($whereMemberTransactionDetailHosting);
			$membersTransactionDetailHosting = $dbHosting->get($tableName)->row_array();
			if ($membersTransactionDetailHosting == null) {
				$dbHosting->insert($tableName, $valAcctHost);
				$iDataSync++;
			}
		}

		if ($dbHosting->trans_status() === FALSE) {
			$dbHosting->trans_rollback();
			$dbHosting->close();
			log_sync_history_detail($tableName, 0, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 0,
				"length_data" => $iDataSync,
			);
		} else {
			$dbHosting->trans_commit();
			$dbHosting->close();
			log_sync_history_detail($tableName, 1, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 1,
				"length_data" => $iDataSync,
			);
		}
	}
}
