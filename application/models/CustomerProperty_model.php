<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerProperty_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function sync($log_sync)
	{
		$tableName = "m_member_properties";
		$dbHosting = connect_db_hosting();
		$dateNow = date('Y-m-d H:i:s');
		$dbHosting->reconnect();

		$dbHosting->trans_begin();

		$membersPropertyLocal = $this->db->get($tableName)->result_array();

		$iDataSync = 0;

		foreach ($membersPropertyLocal as $keyProp => $valProp) {
			$dbHosting->where("nik", $valProp["member_id"]);
			$memberHosting = $dbHosting->get("m_members")->row_array();

			if ($memberHosting == null) continue;

			$memberId = $memberHosting["id"];

			$valPropHost = $valProp;
			$valPropHost["member_id"] = $memberId;

			$whereMemberProp = array(
				"member_id" => $valPropHost["member_id"],
				"property_id" => $valPropHost["property_id"],
			);

			$dbHosting->where($whereMemberProp);
			$membersPropertyHosting = $dbHosting->get($tableName)->row_array();

			if ($membersPropertyHosting != null) {
				$valPropHost["id"] = $membersPropertyHosting["id"];
				$valPropHost["modified"] = $dateNow;
				$dbHosting->where($whereMemberProp);
				$dbHosting->update($tableName, $valPropHost);
			} else {
				$dbHosting->insert($tableName, $valPropHost);
			}

			$iDataSync++;
		}

		if ($dbHosting->trans_status() === FALSE) {
			$dbHosting->trans_rollback();
			$dbHosting->close();
			log_sync_history_detail($tableName, 0, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 0,
				"length_data" => $iDataSync,
			);
		} else {
			$dbHosting->trans_commit();
			$dbHosting->close();
			log_sync_history_detail($tableName, 1, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 1,
				"length_data" => $iDataSync,
			);
		}
	}
}
