<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerAccount_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function sync($log_sync)
	{
		$tableName = "m_savings_accounts";
		$dbHosting = connect_db_hosting();
		$dateNow = date('Y-m-d H:i:s');

		$dbHosting->trans_begin();

		$iDataSync = 0;

		$membersAccountLocal = $this->db->get($tableName)->result_array();

		foreach ($membersAccountLocal as $keyAcct => $valAcct) {
			$dbHosting->where("nik", $valAcct["member_id"]);
			$memberHosting = $dbHosting->get("m_members")->row_array();

			if($memberHosting == null) continue;

			$memberId = $memberHosting["id"];

			$valAcctHost = $valAcct;
			$valAcctHost["member_id"] = $memberId;

			$whereMemberAccount = array(
				"member_id" => $valAcctHost["member_id"],
				"savings_products_id" => $valAcctHost["savings_products_id"]
			);

			$dbHosting->where($whereMemberAccount);
			$membersAccountHosting = $dbHosting->get($tableName)->row_array();

			if ($membersAccountHosting != null) {
				$valAcctHost["id"] = $membersAccountHosting["id"];
				$valAcctHost["modified"] = $dateNow;
				$dbHosting->where(array("id" => $membersAccountHosting["id"]));
				$dbHosting->update($tableName, $valAcctHost);
			} else {
				$dbHosting->insert($tableName, $valAcctHost);
			}

			$iDataSync++;
		}

		if ($dbHosting->trans_status() === FALSE) {
			$dbHosting->trans_rollback();
			$dbHosting->close();
			log_sync_history_detail($tableName, 0, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 0,
				"length_data" => $iDataSync,
			);
		} else {
			$dbHosting->trans_commit();
			$dbHosting->close();
			log_sync_history_detail($tableName, 1, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 1,
				"length_data" => $iDataSync,
			);
		}
	}
}
