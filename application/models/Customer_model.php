<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function register($data)
	{
		$dateNow = date('Y-m-d H:i:s');

		$this->db->trans_begin();

		$field = array(
			'nik' => $data["personalData"]["nik"],
			'name' => $data["personalData"]["name"],
			'place_of_birth' => $data["personalData"]["placeOfBirth"],
			'date_of_birth' => $data["personalData"]["dateOfBirth"],
			'religion_id' => $data["personalData"]["religion"],
			'occupation_id' => $data["personalData"]["occupation"],
			'avrg_salary_id' => $data["personalData"]["averageSalaryPerMonth"],
			'marital_id' => $data["personalData"]["maritalStatus"],
			'address' => $data["personalData"]["address"],
			'addr_province_id' => $data["personalData"]["province"],
			'addr_city_id' => $data["personalData"]["regency"],
			'addr_district_id' => $data["personalData"]["district"],
			'addr_village_id' => $data["personalData"]["village"],
			'addr_rt' => isset($data["personalData"]["rt"]) ? $data["personalData"]["rt"] : "0",
			'addr_rw' => isset($data["personalData"]["rw"]) ? $data["personalData"]["rw"] : "0",
			'lama_tinggal' => isset($data["personalData"]["longStay"]) ? $data["personalData"]["longStay"] : "0",
			'tlp_number' => isset($data["personalData"]["homeNumber"]) ? $data["personalData"]["homeNumber"] : "",
			'hp_number' => isset($data["personalData"]["phoneNumber"]) ? $data["personalData"]["phoneNumber"] : "",
			'old_address' => isset($data["personalData"]["otherAddress"]) ? $data["personalData"]["otherAddress"] : "",
			'alasan_pindah' => isset($data["personalData"]["reasonOtherAddress"]) ? $data["personalData"]["reasonOtherAddress"] : "",
			'family_relation_id' => $data["relationship"]["relationFamily"],
			'family_name' => $data["relationship"]["nameFamily"],
			'family_occupation_id' => $data["relationship"]["occupationFamily"],
			'family_avrg_salary_id' => $data["relationship"]["averageSalaryPerMonthFamily"],
			'family_nik' => $data["relationship"]["nikFamily"],
			'family_tanggungan' => isset($data["relationship"]["amountDependent"]) ? $data["relationship"]["amountDependent"] : "0",
			'status' => 1,
			'created' => $dateNow,
		);

		if (isset($data["saving"])) {
			$field["status_member"] = 1;
		} else {
			$field["status_member"] = 0;
		}

		$this->db->insert('m_members', $field);

		$ownerProperty = isset($data["ownerProperty"]) ? $data["ownerProperty"] : array();

		$keysProp = array_keys($ownerProperty);

		foreach ($keysProp as $keyProp => $valueProp) {
			$vProp = explode("_", $valueProp);

			$this->db->where('member_id', $field["nik"]);
			$this->db->where('property_id', $vProp[1]);
			$isExistProp = $this->db->get('m_member_properties')->row_array();

			if ($isExistProp != null) {
				continue;
			}

			$dataPropInsert = array(
				"member_id" => $field["nik"],
				"property_id" => $vProp[1],
				"value" => $ownerProperty[$valueProp],
				"status" => 1,
				'created' => $dateNow,
			);

			$this->db->where('id', $vProp[1]);
			$prop = $this->db->get("m_properties")->row_array();

			if (isset($ownerProperty[$valueProp]) && $ownerProperty[$valueProp] != "") {
				if ($prop["type_input"] == "checkbox" || $prop["type_input"] == "radio") {
					if ($ownerProperty[$valueProp] == "1") $this->db->insert('m_member_properties', $dataPropInsert);
				} else {
					$this->db->insert('m_member_properties', $dataPropInsert);
				}
			}
		}

		if (isset($data["saving"])) {
			$detail = array();

			if (isset($data["saving"]["pokok"])) {
				$balance = $data["saving"]["pokok"];
				$this->join_new_saving($dateNow, $field["nik"], 1, $balance);
				array_push($detail, array(
					"savingToPay" => 1,
					"amountToPay" => $balance,
				));
			}
			if (isset($data["saving"]["wajib"])) {
				$balance = $data["saving"]["wajib"];
				$this->join_new_saving($dateNow, $field["nik"], 2, $balance);
				array_push($detail, array(
					"savingToPay" => 2,
					"amountToPay" => $balance,
				));
			}
			if(count($detail) > 0) {
				$this->transaction_in($dateNow, $field["nik"], $detail);
			}
		}


		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
	}

	public function join_new_saving($dateNow, $member_id, $savings_products_id, $balance) {
		$dataSaving = array(
			"member_id" => $member_id,
			"savings_products_id" => $savings_products_id,
			"balance" => $balance,
			"created_by" => UserLogged()->id,
			"created" => $dateNow,
			"status" => 1
		);

		$this->db->insert("m_savings_accounts", $dataSaving);
	}

	public function mappingAmountTranDetail($item)
	{
		return $item["amountToPay"];
	}

	public function transaction_in($dateNow, $member_id, array $detail)
	{
		$data = array(
			"member_id" => $member_id,
			"tran_date" => $dateNow,
			"tran_type" => "in",
			"total_amount" => 0,
			"created" => $dateNow
		);

		if (count($detail) > 0) {
			$amountArray = array_map(array($this, 'mappingAmountTranDetail'), $detail);
			$totalAmount = array_sum($amountArray);

			$data["total_amount"] = $totalAmount;
		}

		$this->db->insert("m_saving_transaction", $data);

		foreach ($detail as $det => $d) {
			$dataDetail = array(
				"member_id" => $member_id,
				"savings_products_id" => $d["savingToPay"],
				"amount" => $d["amountToPay"],
				"created" => $dateNow
			);

			$this->db->insert("m_saving_transaction_detail", $dataDetail);
		}

	}

	public function transaction_out($dateNow, $member_id, array $detail)
	{
		$data = array(
			"member_id" => $member_id,
			"tran_date" => $dateNow,
			"tran_type" => "out",
			"total_amount" => 0,
			"created" => $dateNow
		);

		if (count($detail) > 0) {
			$amountArray = array_map(array($this, 'mappingAmountTranDetail'), $detail);
			$totalAmount = array_sum($amountArray);

			$data["total_amount"] = $totalAmount;
		}

		$this->db->insert("m_saving_transaction", $data);

		foreach ($detail as $det => $d) {
			$dataDetail = array(
				"member_id" => $member_id,
				"savings_products_id" => $d["savingToPay"],
				"amount" => $d["amountToPay"],
				"created" => $dateNow
			);

			$this->db->insert("m_saving_transaction_detail", $dataDetail);
		}

	}

	public function sync($log_sync)
	{
		$tableName = "m_members";
		$dbHosting = connect_db_hosting();
		$dbHosting->trans_begin();

		$dateNow = date("Y-m-d H:i:s");
		$membersLocal = $this->db->get($tableName)->result_array();

		$iDataSync = 0;

		if (count($membersLocal) > 0) {
			foreach ($membersLocal as $key => $val) {
				$valHost = $val;
				$whereMemberByNik = array(
					"nik" => $val["nik"],
				);

				$dbHosting->where($whereMemberByNik);
				$memberHosting = $dbHosting->get($tableName)->row_array();
				if ($memberHosting != null) {
					$valHost["id"] = $memberHosting["id"];
					$valHost["modified"] = $dateNow;
					$dbHosting->where($whereMemberByNik);
					$dbHosting->update($tableName, $valHost);
				} else {
					$dbHosting->insert($tableName, $valHost);
				}

				$iDataSync++;
			}
		}

		if ($dbHosting->trans_status() === FALSE) {
			$dbHosting->trans_rollback();
			$dbHosting->close();
			log_sync_history_detail($tableName, 0, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 0,
				"length_data" => $iDataSync,
			);
		} else {
			$dbHosting->trans_commit();
			$dbHosting->close();
			log_sync_history_detail($tableName, 1, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 1,
				"length_data" => $iDataSync,
			);
		}
	}
}
