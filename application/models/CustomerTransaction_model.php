<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerTransaction_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function sync($log_sync)
	{
		$tableName = "m_saving_transaction";
		$dbHosting = connect_db_hosting();
		$dateNow = date('Y-m-d H:i:s');

		$dbHosting->trans_begin();

		$iDataSync = 0;

		$membersTransactionLocal = $this->db->get($tableName)->result_array();

		foreach ($membersTransactionLocal as $keyAcct => $valAcct) {
			$dbHosting->where("nik", $valAcct["member_id"]);
			$memberHosting = $dbHosting->get("m_members")->row_array();

			if($memberHosting == null) continue;

			$memberId = $memberHosting["id"];

			$valAcctHost = $valAcct;
			$valAcctHost["member_id"] = $memberId;

			$whereMemberTransaction = array(
				"member_id" => $valAcctHost["member_id"],
				"tran_date" => $valAcctHost["tran_date"],
				"tran_type" => $valAcctHost["tran_type"],
			);

			$dbHosting->where($whereMemberTransaction);
			$membersTransactionHosting = $dbHosting->get($tableName)->row_array();

			if ($membersTransactionHosting == null) {
				unset($valAcctHost["id"]);
				$dbHosting->insert($tableName, $valAcctHost);
			}

			$iDataSync++;
		}

		if ($dbHosting->trans_status() === FALSE) {
			$dbHosting->trans_rollback();
			$dbHosting->close();
			log_sync_history_detail($tableName, 0, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 0,
				"length_data" => $iDataSync,
			);
		} else {
			$dbHosting->trans_commit();
			$dbHosting->close();
			log_sync_history_detail($tableName, 1, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 1,
				"length_data" => $iDataSync,
			);
		}
	}
}
