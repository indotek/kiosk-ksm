<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
	var $dbHosting;

	public function __construct()
	{
		parent::__construct();
	}

	public function check_users_login($u, $p)
	{
		$this->db->select("users.*, role.name as rolename");

		$this->db->where("email", $u, true);
		$this->db->where("password", $p, true);

		$this->db->join("role", "users.role_id = role.id");
		$this->db->from("users");

		$data = $this->db->get();
		return $data->row();
	}

	public function sync($log_sync)
	{
		$dateNow = date('Y-m-d H:i:s');
		$this->dbHosting = connect_db_hosting();

		$this->db->trans_begin();
		$this->dbHosting->trans_begin();

		$tableName = "users";
		$x = $this->dbHosting->get($tableName);

		$iDataSync = 0;

		if ($x->num_rows() > 0) {
			$q = $x->result_array();
			foreach ($q as $key => $val) {
				$whereId = array("id" => $val["id"]);
				$this->db->where($whereId);
				$dataLocal = $this->db->get($tableName)->row_array();

				$valLocal = array(
					"id" => $val["id"],
					"role_id" => isset($val["aro_id"]) ? $val["aro_id"] : null,
					"email" => $val["email"],
					"firstname" => $val["firstname"],
					"created" => $val["created"],
					"modified" => isset($val["modified"]) ? $val["modified"] : null,
					"lastname" => isset($val["lastname"]) ? $val["lastname"] : null,
					"password" => $val["password"],
					"status" => isset($val["status"]) ? $val["status"] : null,
				);

				if ($dataLocal != null) {
					$this->db->where($whereId);
					$this->db->update($tableName, $valLocal);
				} else {
					$this->db->insert($tableName, $valLocal);
				}

				if (!($this->db->trans_status() === FALSE)) {
					$iDataSync++;
				}
			}
			$autoIncrementHosting = get_ai_hosting($tableName);

			if ($autoIncrementHosting != null && isset($autoIncrementHosting["AUTO_INCREMENT"])) {
				update_ai_local($tableName, $autoIncrementHosting["AUTO_INCREMENT"]);
			}
		}

		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			$this->dbHosting->close();
			log_sync_history_detail($tableName, 0, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => 0,
				"length_data" => array("failed" => $iDataSync, "success" => 0),
			);
		} else {
			$this->db->trans_commit();
			$this->dbHosting->close();
			log_sync_history_detail($tableName, 1, $dateNow, $iDataSync, $log_sync);
			return array(
				"status" => ($iDataSync > 0) ? 1 : 3,
				"length_data" => array("success" => $iDataSync, "failed" => 0),
			);
		}
	}
}
