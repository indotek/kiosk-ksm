<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Master_model extends CI_Model
{
	var $dbHosting;

	public function __construct()
	{
		parent::__construct();
	}

	public function sync($log_sync)
	{
		$this->dbHosting = connect_db_hosting();

		$listTableHosting = $this->dbHosting->list_tables();
		$listTableLocal = $this->db->list_tables();

		$listTableLocal = array_filter($listTableLocal, 'search_table_local');

		$iTableDataSync = 0;

		$tableSuccessSync = array();
		$tableFailedSync = array();

		$iAllDataSync = 0;

		foreach ($listTableLocal as $key => $val) {
			$tableName = $val;
			if (in_array($tableName, $listTableHosting)) {
				$this->db->trans_begin();
				$this->db->trans_strict(FALSE);

				$x = $this->dbHosting->get($tableName);

				$iDataSync = 0;
				$dateSync = date('Y-m-d H:i:s');

				if ($x->num_rows() > 0) {
					$q = $x->result_array();
					foreach ($q as $key => $val) {
						$whereId = array("id" => $val["id"]);
						$this->db->where($whereId);
						$dataLocal = $this->db->get($tableName)->row_array();
						$valLocal = $val;

						if ($dataLocal != null) {
							$this->db->where($whereId);
							$this->db->update($tableName, $valLocal);
						} else {
							$this->db->insert($tableName, $valLocal);
						}

						if (!($this->db->trans_status() === FALSE)) {
							$iDataSync++;
						}
					}
					$autoIncrementHosting = get_ai_hosting($tableName);

					if ($autoIncrementHosting != null && isset($autoIncrementHosting["AUTO_INCREMENT"])) {
						update_ai_local($tableName, $autoIncrementHosting["AUTO_INCREMENT"]);
					}
				}

				if ($iDataSync > 0) {
					$iTableDataSync++;
				}

				if ($this->db->trans_status() === FALSE
				) {
					$this->db->trans_rollback();
					array_push($tableFailedSync, array(
						"status" => false,
						"name" => $tableName,
					));
					log_sync_history_detail($tableName, 0, $dateSync, $iDataSync, $log_sync);
				} else {
					$this->db->trans_commit();
					array_push($tableSuccessSync, array(
						"status" => true,
						"name" => $tableName,
					));
					log_sync_history_detail($tableName, 1, $dateSync, $iDataSync, $log_sync);
				}

				$iAllDataSync += $iDataSync;
			}
		}

		$this->dbHosting->close();

		$ret = array(
			"status" => 0,
			"data" => array(
				"failed" => $tableFailedSync,
				"success" => $tableSuccessSync,
			),
			"length_data" => array(
				"failed" => count($tableFailedSync),
				"success" => count($tableSuccessSync),
			),
			"all_length_data" => $iAllDataSync
		);

		if ($iTableDataSync == 0) {
			$ret["status"] = 3;
		} else {
			if (count($tableFailedSync) == 0 && count($tableSuccessSync) > 0) {
				$ret["status"] = 1;
			} else if (count($tableFailedSync) > 0 && count($tableSuccessSync) > 0) {
				$ret["status"] = 2;
			}
		}
		return $ret;
	}
}
