<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php echo $title; ?></title>
</head>
<?php
foreach ($css as $key => $value) {
	?>
	<link rel="stylesheet" href="<?= asset_url($value); ?>">
	<?php
}
?>
<script>
	var base_url = "<?= base_url(); ?>";
	var site_url = "<?= site_url("/"); ?>";
	var api_url = "<?= site_url("api/"); ?>";
	var asset_url = "<?= asset_url(); ?>";
	var language = "<?= $this->session->userdata('language'); ?>";
	var languagePopup = <?= json_encode(lang('popup')); ?>;
	var languageForm = <?= json_encode(lang('form')); ?>;
</script>
<?php
foreach ($js["before"] as $key => $value) {
	?>
	<script src="<?= asset_url($value); ?>"></script>
	<?php
}
?>

<body class="white-skin">
<main>
	<?= $content; ?>
</main>
<?php
foreach ($js["after"] as $key => $value) {
	?>
	<script src="<?= asset_url($value); ?>"></script>
<?php
if ($value == "mdb/js/mdb.js" || $value == "mdb/js/mdb.min.js") {
?>
	<script>
		var loadingBeforeReady = showProgress();
		$(window).on('load', function () {
			hideProgress(loadingBeforeReady);
		});
	</script>
	<?php
}
}
?>
</body>

</html>
