<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="card-error">
	<div class="card card-cascade narrower">
		<div class="view view-cascade gradient-card-header peach-gradient">
			<h2 class="card-header-title">
				<?= $heading ?>
			</h2>
		</div>
		<div class="card-body card-body-cascade">
			<div class="text-center">
				<p class="red-text"><?= $message ?></p>
				<button type="button" class="btn btn-outline-danger waves-effect btn-rounded"
						data-redirect="<?= get_previous_url() ?>">Go
				</button>
			</div>
		</div>
	</div>
</section>
