<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $title; ?></title>
</head>
<?php
foreach ($css as $key => $value) {
	?>
	<link rel="stylesheet" href="<?= asset_url($value); ?>">
	<?php
}
?>
<script>
	var base_url = "<?= base_url(); ?>";
	var site_url = "<?= site_url("/"); ?>";
	var api_url = "<?= site_url("api/"); ?>";
	var asset_url = "<?= asset_url(); ?>";
	var language = "<?= $this->session->userdata('language'); ?>";
	var languagePopup = <?= json_encode(lang('popup')); ?>;
	var languageForm = <?= json_encode(lang('form')); ?>;
	var isOnline = <?= check_connection_database(); ?>;
</script>
<?php
foreach ($js["before"] as $key => $value) {
	?>
	<script src="<?= asset_url($value); ?>"></script>
	<?php
}
?>

<body class="fixed-sn white-skin">
<div class="fixed-action-btn-main" style="bottom: 45px; right: 24px;">
	<a class="btn-floating red">
		<i class="fas fa-ellipsis-v"></i>
	</a>

	<ul class="list-unstyled">
		<li><a class="btn-floating red" id="btnSync"><i class="fas fa-sync"></i></a></li>
		<li><a class="btn-floating red" id="btnToCustomerRegister"><i class="fas fa-user-plus"></i></a></li>
	</ul>
</div>
<header>
	<div id="slide-out" class="side-nav fixed sn-bg-1">
		<ul class="custom-scrollbar">
			<li class="logo-sn waves-effect py-3">
				<?= $this->load->view('component-template/menu-logo', null, true); ?>
			</li>
			<li>
				<?= $this->load->view('component-template/menu-search', null, true); ?>
			</li>
			<li>
				<?= $this->load->view('component-template/menu-sidebar', null, true); ?>
			</li>
		</ul>
		<div class="sidenav-bg mask-strong"></div>
	</div>
	<?= $this->load->view('component-template/menu-header', null, true); ?>
</header>
<main>
	<div class="container-fluid">
		<?= $content ?>
	</div>
</main>

<div class="modal fade" id="modalApi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="true">
	<div class="modal-dialog modal-notify modal-fluid modal-full-height modal-top" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<p class="heading lead" id="title-modal-api"></p>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="white-text">&times;</span>
				</button>
			</div>

			<div class="modal-body">
				<div class="text-center">
					<i class="fas fa-4x mb-3 animated rotateIn" id="icon-modal-api"></i>
					<p id="message-modal-api"></p>
				</div>
			</div>
			<div class="modal-footer justify-content-end">
				<a type="button" class="btn btn-outline-danger waves-effect" data-dismiss="modal">Close</a>
			</div>
		</div>
	</div>
</div>
<?php
foreach ($js["after"] as $key => $value) {
	?>
	<script src="<?= asset_url($value); ?>"></script>
<?php
if ($value == "mdb/js/mdb.js" || $value == "mdb/js/mdb.min.js") {
?>
	<script>
		var loadingBeforeReady = showProgress();
		$(window).on('load', function () {
			hideProgress(loadingBeforeReady);
		});
	</script>
	<?php
}
}
?>
</body>

</html>
