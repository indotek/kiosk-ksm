<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php
$langWelcome = lang("welcome");
?>

<section class="mb-5">
	<div class="card transparent text-center z-depth-2">
		<div class="card-body">
			<p>
			<?php
			$langWelcome = str_replace("{user}", UserLogged()->firstname, $langWelcome);
			$langWelcome = str_replace("{role}", UserLogged()->rolename, $langWelcome);
			echo $langWelcome;
			?>
			</p>
		</div>
	</div>
</section>

<?= $this->load->view('component/search-customer-nik', null, true); ?>
