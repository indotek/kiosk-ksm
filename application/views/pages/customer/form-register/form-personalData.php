<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 09:02
 */
?>
<?php
$options_religion = get_options_data("m_religions", "id", "value");
$options_marital = get_options_data("m_maritals", "id", "value");
$options_occupation = get_options_data("m_occupations", "id", "value");
$options_avg_salary = get_options_data("m_average_salaries", "id", "value");
$options_province = get_options_data("m_addr_provinces", "id", "name");
?>
<form autocomplete="off" id="form-<?= $tab_id ?>" method="post" action="" class="form form-validate">
	<!-- Data Personal -->
	<div class="row">
		<div class="col-12">
			<div class="md-form">
				<input type="text" id="input-nik" class="form-control inputmask" name="input-nik" required
					   data-inputmask="'mask': '9', 'repeat': 21"
					   minlength="10"
					   maxlength="20"
					   data-validation-digits
					   data-validation-niknotregistered>
				<label for="input-nik"><?= $tab_lang["input-nik"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="md-form">
				<input type="text" id="input-name" class="form-control" name="input-name" required minlength="5"
					   maxlength="50">
				<label for="input-name"><?= $tab_lang["input-name"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-4">
			<div class="md-form">
				<input type="text" id="input-placeOfBirth" name="input-placeOfBirth" class="form-control" required>
				<label for="input-placeOfBirth"><?= $tab_lang["input-placeOfBirth"] ?></label>
			</div>
		</div>
		<div class="col-8">
			<div class="md-form">
				<input type="text" id="input-dateOfBirth" class="form-control datepicker" name="input-dateOfBirth"
					   required
					   data-max='<?= date_to_array_string(date('Y-n-j', strtotime(date('Y-m-d') . ' -16 year')), '-'); ?>'>
				<label for="input-dateOfBirth"><?= $tab_lang["input-dateOfBirth"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "religion",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => $options_religion,
			), true); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "maritalStatus",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => $options_marital,
			), true); ?>
		</div>
	</div>


	<!-- Data Occupation -->
	<div class="row">
		<div class="col-6">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "occupation",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => $options_occupation,
			), true); ?>
		</div>
		<div class="col-6">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "averageSalaryPerMonth",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => $options_avg_salary,
			), true); ?>
		</div>
	</div>


	<!-- Data Address -->
	<div class="row">
		<div class="col-12">
			<div class="md-form">
				<textarea type="text" id="textarea-address" class="md-textarea form-control"
						  required name="textarea-address"
						  rows="3"></textarea>
				<label for="textarea-address"><?= $tab_lang["textarea-address"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-3">
			<div class="md-form">
				<input type="text" id="input-rt" name="input-rt" class="form-control inputmask" data-validation-digits data-inputmask="'mask': '9', 'repeat': 3" >
				<label for="input-rt"><?= $tab_lang["input-rt"] ?></label>
			</div>
		</div>
		<div class="col-3">
			<div class="md-form">
				<input type="text" id="input-rw" name="input-rw" class="form-control inputmask" data-validation-digits data-inputmask="'mask': '9', 'repeat': 3">
				<label for="input-rw"><?= $tab_lang["input-rw"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-3">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "province",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => $options_province,
			), true); ?>
		</div>
		<div class="col-3">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "regency",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => array(),
			), true); ?>
		</div>
		<div class="col-3">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "district",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => array(),
			), true); ?>
		</div>
		<div class="col-3">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "village",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => array(),
			), true); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-3">
			<div class="md-form">
				<input type="number" id="input-longStay" name="input-longStay" class="form-control"
					   min="0"
					   data-validation-digits>
				<label for="input-longStay"><?= $tab_lang["input-longStay"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<div class="md-form">
				<input type="text" id="input-homeNumber" name="input-homeNumber" class="form-control inputmask"
					   data-inputmask="'mask': '9999-9999-9999'">
				<label for="input-homeNumber"><?= $tab_lang["input-homeNumber"] ?></label>
			</div>
		</div>
		<div class="col-6">
			<div class="md-form">
				<input type="text" id="input-phoneNumber" name="input-phoneNumber" class="form-control inputmask"
					   data-inputmask="'mask': '9999-9999-9999'">
				<label for="input-phoneNumber"><?= $tab_lang["input-phoneNumber"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-8">
			<div class="md-form">
				<textarea type="text" id="textarea-otherAddress" name="textarea-otherAddress"
						  class="md-textarea form-control"
						  rows="3"></textarea>
				<label for="textarea-otherAddress"><?= $tab_lang["textarea-otherAddress"] ?></label>
			</div>
		</div>
		<div class="col-4">
			<div class="md-form">
				<textarea type="text" id="textarea-reasonOtherAddress" name="textarea-reasonOtherAddress"
						  class="md-textarea form-control"
						  rows="3"></textarea>
				<label for="textarea-reasonOtherAddress"><?= $tab_lang["textarea-reasonOtherAddress"] ?></label>
			</div>
		</div>
	</div>
</form>
