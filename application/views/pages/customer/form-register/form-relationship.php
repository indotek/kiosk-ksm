<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 09:02
 */
?>
<?php
$options_relation = get_options_data("m_family_relations", "id", "value");
$options_occupation = get_options_data("m_occupations", "id", "value");
$options_avg_salary = get_options_data("m_average_salaries", "id", "value");
?>
<form autocomplete="off" id="form-<?= $tab_id ?>" method="post" action="" class="form form-validate">
	<div class="row">
		<div class="col-12">
			<div class="md-form">
				<input type="text" id="input-nikFamily" name="input-nikFamily" class="form-control inputmask" required
					   data-inputmask="'mask': '9', 'repeat': 20"
					   minlength="10"
					   maxlength="20"
					   data-validation-digits>
				<label for="input-nikFamily"><?= $tab_lang["input-nikFamily"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="md-form">
				<input type="text" id="input-nameFamily" name="input-nameFamily" class="form-control" required>
				<label for="input-nameFamily"><?= $tab_lang["input-nameFamily"] ?></label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "relationFamily",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => $options_relation,
			), true); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "occupationFamily",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => $options_occupation,
			), true); ?>
		</div>
		<div class="col-6">
			<?= $this->load->view('component/dropdown-select2', array(
				"id" => "averageSalaryPerMonthFamily",
				"validation" => array("required"),
				"placeholder" => true,
				"options" => $options_avg_salary,
			), true); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class="md-form">
				<input type="number" id="input-amountDependent" name="input-amountDependent" class="form-control" min="0" value="0"
					   data-validation-digits>
				<label for="input-amountDependent"><?= $tab_lang["input-amountDependent"] ?></label>
			</div>
		</div>
	</div>
</form>
