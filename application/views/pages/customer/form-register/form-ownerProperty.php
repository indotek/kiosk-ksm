<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 09:02
 */
?>
<form autocomplete="off" id="form-<?= $tab_id; ?>" method="post" action="" class="form form-validate">
	<?php
	$this->db->where('status', 1);
	$category_prop = $this->db->get("m_property_categories")->result_array();

	foreach ($category_prop as $key_category => $row_category) {
		?>
		<div class="row mb-3">
			<div class="col-4">
				<label><?= $row_category["value"]; ?></label>
			</div>
			<div class="col-8">
				<?php
				$this->db->where('status', 1);
				$this->db->where('property_categories_id', $row_category["id"]);
				$prop = $this->db->get("m_properties")->result_array();

				$col = "col";

				$type_for_one_row = array("checkbox", "radio");

				if (count($prop) > 0) {
					if (!in_array($prop[0]["type_input"], $type_for_one_row)) {
						$col = "col-12";
					}
				}
				?>
				<div class="row">
					<?php
					foreach ($prop as $key_prop => $row_prop) {
						if ($row_prop["type_input"] == "checkbox") {
							?>
							<div class="col">
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input"
										   id="cb-<?= $tab_id . '_' . $row_prop["id"]; ?>"
										   name="cb-<?= $tab_id . '_' . $row_prop["id"]; ?>">
									<label class="custom-control-label"
										   for="cb-<?= $tab_id . '_' . $row_prop["id"]; ?>"><?= $row_prop["value"]; ?></label>
								</div>
							</div>
							<?php
						} else if ($row_prop["type_input"] == "radio") {
							?>
							<div class="col">
								<div class="custom-control custom-radio">
									<input type="radio" class="custom-control-input"
										   id="rb-<?= $tab_id . '_' . $row_prop["id"]; ?>"
										   name="rb-<?= $tab_id . '_' . $row_category["id"]; ?>">
									<label class="custom-control-label"
										   for="rb-<?= $tab_id . '_' . $row_prop["id"]; ?>"><?= $row_prop["value"]; ?></label>
								</div>
							</div>
							<?php
						} else if ($row_prop["type_input"] == "text" || $row_prop["type_input"] == "number") {
							?>
							<div class="col-12">
								<div class="md-form input-group mt-0 mb-1">
									<div class="input-group-prepend">
										<span class="md-addon input-group-text"><?= $row_prop["value"]; ?></span>
									</div>
									<input type="<?= $row_prop["type_input"] ?>" class="form-control"
										   id="input-<?= $tab_id . '_' . $row_prop["id"]; ?>"
										   name="input-<?= $tab_id . '_' . $row_prop["id"]; ?>"
										   aria-label="<?= $row_prop["value"]; ?>" <?= $row_prop["type_input"] == "number" ? "min='0'" : ""; ?>>
									<?php
									if (isset($row_prop["unit"]) && $row_prop["unit"] != "") {
										?>
										<div class="input-group-append">
											<span class="md-addon input-group-text"><?= $row_prop["unit"]; ?></span>
										</div>
										<?php
									}
									?>
								</div>
							</div>
							<?php
						} else if ($row_prop["type_input"] == "textarea") {
							?>
							<div class="col-12">
								<div class="">
									<textarea class="md-textarea form-control" rows="3"
											  id="textarea-<?= $tab_id . '_' . $row_prop["id"]; ?>"
											  name="textarea-<?= $tab_id . '_' . $row_prop["id"]; ?>"
											  placeholder="......................"></textarea>
								</div>
							</div>
							<?php
						}
					}
					?>
				</div>

			</div>
		</div>
		<?php
	}
	?>
</form>
