<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<form autocomplete="off" id="form-<?= $id ?>" method="post" action="" class="form form-validate">
	<div class="row">
		<div class="col-12">
			<div class="md-form">
				<textarea type="text" id="textarea-address" class="md-textarea form-control"
						  required name="textarea-address"
						  rows="3"></textarea>
				<label for="textarea-address">Address (ex: Address Block Number)</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-3">
			<div class="md-form">
				<input type="text" id="input-rt" name="input-rt" class="form-control" data-validation-digits>
				<label for="input-rt">RT</label>
			</div>
		</div>
		<div class="col-3">
			<div class="md-form">
				<input type="text" id="input-rw" name="input-rw" class="form-control" data-validation-digits>
				<label for="input-rw">RW</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-3">
			<div class="md-form">
				<select class="browser-default select2" id="select-village" name="select-village"
						required
						placeholder="Select Village">
				</select>
				<label for="select-village" class="active">Village</label>
			</div>
		</div>
		<div class="col-3">
			<div class="md-form">
				<select class="browser-default select2" id="select-district" name="select-district"
						required
						placeholder="Select District">
				</select>
				<label for="select-district" class="active">District</label>
			</div>
		</div>
		<div class="col-3">
			<div class="md-form">
				<select class="browser-default select2" id="select-regency" name="select-regency"
						required
						placeholder="Select Regency">
				</select>
				<label for="select-regency" class="active">Regency</label>
			</div>
		</div>
		<div class="col-3">
			<div class="md-form">
				<select class="browser-default select2" id="select-province" name="select-province"
						required
						placeholder="Select Province..">
				</select>
				<label for="select-province" class="active">Province</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-3">
			<div class="md-form">
				<input type="text" id="input-longStay" name="input-longStay" class="form-control"
					   data-validation-digits>
				<label for="input-longStay">Long Stay</label>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<div class="md-form">
				<textarea type="text" id="textarea-otherAddress" name="textarea-otherAddress"
						  class="md-textarea form-control"
						  rows="3"></textarea>
				<label for="textarea-otherAddress">Other Address (ex: Address Block Number RT/RW Village
					District,
					Regency, Province)</label>
			</div>
		</div>
		<div class="col-6">
			<div class="md-form">
				<textarea type="text" id="textarea-reasonOtherAddress" name="textarea-reasonOtherAddress"
						  class="md-textarea form-control"
						  rows="3"></textarea>
				<label for="textarea-reasonOtherAddress">Reason Other Address</label>
			</div>
		</div>
	</div>
</form>
