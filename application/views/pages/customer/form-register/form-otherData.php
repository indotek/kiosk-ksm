<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 09:02
 */
?>
<form autocomplete="off" id="form-<?= $tab_id; ?>" method="post" action="" class="form form-validate">
	<div class="row">
		<div class="col-12">
			<div class="md-form">
				<textarea type="text" id="textarea-otherData" name="textarea-otherData"
						  class="md-textarea form-control"
						  rows="5"></textarea>
				<label for="textarea-otherData"><?= $tab_lang["textarea-otherData"] ?></label>
			</div>
		</div>
	</div>
</form>
