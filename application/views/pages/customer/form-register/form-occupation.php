<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<form autocomplete="off" id="form-<?= $id ?>" method="post" action="" class="form form-validate">
	<div class="row">
		<div class="col-6">
			<div class="md-form">
				<select class="browser-default select2" id="select-occupation" name="select-occupation" required
						placeholder="Select Occupation">
				</select>
				<label for="select-occupation" class="active">Occupation</label>
			</div>
		</div>
		<div class="col-6">
			<div class="md-form">
				<select class="browser-default select2" id="select-averageSalaryPerMonth"
						name="select-averageSalaryPerMonth" required
						placeholder="Select Average Salary Per Month">
				</select>
				<label for="select-averageSalaryPerMonth" class="active">Average Salary</label>
			</div>
		</div>
	</div>
</form>
