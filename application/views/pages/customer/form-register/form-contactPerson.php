<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<form autocomplete="off" id="form-<?= $id ?>" method="post" action="" class="form form-validate">
	<div class="row">
		<div class="col-6">
			<div class="md-form">
				<input type="text" id="input-homeNumber" name="input-homeNumber" class="form-control"
					   data-validation-phoneID>
				<label for="input-homeNumber">Home Number (ex : 0341-xxxx-xxxx)</label>
			</div>
		</div>
		<div class="col-6">
			<div class="md-form">
				<input type="text" id="input-phoneNumber" name="input-phoneNumber" class="form-control"
					   data-validation-phoneID>
				<label for="input-phoneNumber">Phone Number (ex : 08xx-xxxx-xxxx)</label>
			</div>
		</div>
	</div>
</form>
