<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 09:02
 */

$stepper = lang("tab_form");
$notify_registration = lang("notify_registration");
?>
<script>
	var langPopupRegistration = <?= json_encode($notify_registration)?>;
	var simpananPokokWajib = <?= json_encode($savingProducts); ?>;
</script>
<section id="sectionFormCustomerRegistration">
	<div class="card card-cascade narrower">
		<div class="view view-cascade gradient-card-header peach-gradient py-2 mx-4 mb-3">
			<h2 class="card-header-title mb-2"><?= lang("tab_title") ?></h2>
		</div>
		<div class="card-body card-body-cascade">
			<div class="row">
				<div class="col-md-12">
					<ul class="stepper stepper-horizontal" data-target="#stepperRegistration">
						<?php
						for ($i = 0; $i < count($stepper); $i++) {
							$step = $stepper[$i];
							?>
							<li>
								<a href="#" data-target="<?= $step["tab_id"]; ?>">
									<span
										class="circle"><?= (isset($step["tab_no"])) ? $step["tab_no"] : ($i + 1); ?></span>
									<span class="label"><?= $step["tab_title"]; ?></span>
								</a>
							</li>
							<?php
						}
						?>
					</ul>
				</div>
			</div>
			<div class="stepper-container-content" id="stepperRegistration">
				<?php
				for ($i = 0; $i < count($stepper); $i++) {
					$step = $stepper[$i];
					$step["tab_lang"] = lang("form_" . $step["tab_id"]);
					?>
					<div class="stepper-content fade" id="stepper-content-<?= $step["tab_id"] ?>">
						<?= $this->load->view('pages/customer/form-register/form-' . $step["tab_template"], $step, true); ?>
					</div>
					<?php
				}
				?>
				<div class="row">
					<div class="col-6">
						<div class="text-left stepper-processor fade">
							<button type="button" id="btnProcess"
									class="btn btn-outline-success waves-effect btn-rounded">Process <i
									class="fas fa-check ml-2"></i></button>
							<button type="button" id="btnCancel"
									class="btn btn-outline-danger waves-effect btn-rounded">Cancel <i
									class="fas fa-times ml-2"></i></button>
						</div>
					</div>
					<div class="col-6">
						<div class="text-right stepper-navigator">
							<button type="button" id="btnBack"
									class="btn btn-outline-default waves-effect btn-rounded fade"><i
									class="fas fa-arrow-circle-left mr-2"></i> Back
							</button>
							<button type="button" id="btnNext"
									class="btn btn-outline-primary waves-effect btn-rounded fade">Next <i
									class="fas fa-arrow-circle-right ml-2"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modalRegistration" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-notify" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<p class="heading lead modal-title"></p>
					<!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>-->
				</div>

				<div class="modal-body">
					<div class="text-center">
						<i class="fas fa-4x mb-3 modal-icon"></i>
					</div>

					<p id="modal-message"></p>

					<form id="formPayment">
						<div class="row">
							<div class="col-12">
								<div class="md-form md-outline">
									<input type="text" id="input-nominal-simpanan-pokok" class="form-control inputmask pt-3" name="input-nominal-simpanan-pokok" data-inputmask="'alias':'rp', 'min':<?= $savingProducts["pokok"]["initial_deposit"] * (30/100) ?>, 'max':<?= $savingProducts["pokok"]["initial_deposit"].".00" ?>" placeholder="0">
									<label for="input-nominal-simpanan-pokok">Nominal Simpanan Pokok</label>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="md-form md-outline">
									<input type="text" id="input-nominal-simpanan-wajib" class="form-control text-white inputmask grey pt-3" name="input-nominal-simpanan-wajib" data-inputmask="'alias':'rp'" placeholder="0">
									<label for="input-nominal-simpanan-wajib" class="grey text-white">Nominal Simpanan Wajib</label>
								</div>
							</div>
						</div>
						<div>
							<small>Note : Setuju diartikan menjadi anggota.<br>Tidak Setuju diartikan menjadi non anggota.<br>Status mempengaruhi simpanan & pinjaman yang bisa diambil.</small>
						</div>
					</form>
				</div>
				<div class="modal-footer justify-content-end">
					<button type="button" class="btn btn-outline-danger waves-effect" id="btnDecline">Tidak Setuju</button>
					<button type="button" class="btn btn-outline-success waves-effect" id="btnAccept">Setuju</button>
				</div>
			</div>
		</div>
	</div>
</section>
