<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 13/05/2020
 * Time: 01:52
 */
?>

<script>
	var savingProduct = <?= json_encode($savingProducts); ?>;
</script>
<div class="position-relative h-75">
	<?= $this->load->view('component/search-customer-nik', null, true); ?>
</div>
<section class="d-none fade animated" id="sectionDetailCustomer">
	<div class="card card-cascade narrower">
		<div class="view view-cascade gradient-card-header peach-gradient py-2 mx-4 mb-3">
			<h2 class="card-header-title mb-2"><?= lang("tab_title") ?></h2>
		</div>
		<div class="card-body card-body-cascade">
			<div class="row">
				<div class="col-6">
					<div class="row mb-2">
						<div class="col"><h5>NIK</h5></div>
						<div class="col"><span id="fill-customer-nik"></span></div>
					</div>
					<div class="row mb-2">
						<div class="col"><h5>Nama Lengkap</h5></div>
						<div class="col"><span id="fill-customer-name"></span></div>
					</div>
					<div class="row mb-2 d-none">
						<div class="col"><h5>Jumlah Jenis Simpanan</h5></div>
						<div class="col"><span id="fill-customer-totalSaving"></span></div>
					</div>
					<hr>
				</div>
				<div class="col-6">
					<div class="row mb-2">
						<div class="col"><h5>Status Anggota</h5></div>
						<div class="col"><span id="fill-customer-status"></span></div>
					</div>
					<div class="row mb-2">
						<div class="col"><h5>Tanggal Bergabung</h5></div>
						<div class="col"><span id="fill-customer-created"></span></div>
					</div>
					<div class="row mb-2 d-none">
						<div class="col"><h5>Saldo Saat Ini</h5></div>
						<div class="col"><span id="fill-customer-totalBalance"></span></div>
					</div>
					<div class="row mb-2 d-none">
						<div class="col"><h5>Jumlah Jenis Pinjaman</h5></div>
						<div class="col"><span id="fill-customer-totalLoan"></span></div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6">
					<div class="btn-group btn-block">
						<button type="button" class="btn btn-outline-danger btn-rounded waves-effect"
								id="btnTransactionOut">Keluar
						</button>
						<button type="button" class="btn btn-outline-indigo btn-rounded waves-effect" id="btnJoinSaving">
							Buat Simpanan
						</button>
						<button type="button" class="btn btn-outline-success btn-rounded waves-effect"
								id="btnTransactionIn">Masuk
						</button>
					</div>
				</div>
			</div>

			<div class="row">
				<div class="col-6">
					<div class="card card-cascade narrower z-depth-0 mt-5">
						<div
							class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 align-items-center">
							<a class="white-text mx-3">Simpanan</a>
						</div>

						<table id="tableSaving" class="table table-striped" cellspacing="0" width="100%">
							<thead>
							<tr>
								<th>Nama Simpanan</th>
								<th>Saldo</th>
								<th>Initial Deposit</th>
								<th>Hutang</th>
							</tr>
							</thead>
						</table>

					</div>
				</div>
				<div class="col-6">
					<div class="card card-cascade narrower z-depth-0 mt-5 d-none">
						<div
							class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 align-items-center">
							<a class="white-text mx-3">Pinjaman</a>
						</div>

						<table id="tableLoan" class="table table-striped" cellspacing="0" width="100%">
							<thead>
							<tr>
								<th>Nama Simpanan</th>
								<th>Saldo</th>
								<th>Initial Deposit</th>
								<th>Hutang</th>
							</tr>
							</thead>
						</table>

					</div>

				</div>
			</div>
		</div>
	</div>
</section>


<div class="modal fade" id="modalTransaction" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="true" data-tranType="in">
	<div class="modal-dialog modal-notify modal-lg modal-info" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<p class="heading lead">Simpanan Transaksi <b class="title-type-transaction">Masuk</b></p>
			</div>

			<div class="modal-body">
				<form id="formTransaction">
					<div class="row">
						<div class="col-12">
							<?= $this->load->view('component/dropdown-select2', array(
								"id" => "type-saving",
								"label" => "Jenis Simpanan",
								"validation" => array("required"),
								"placeholder" => true,
								"options" => array(),
							), true); ?>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="md-form md-outline">
								<input type="text" id="input-nominal" class="form-control inputmask pt-3"
									   name="input-nominal" data-inputmask="'alias':'rp'" placeholder="0">
								<label for="input-nominal">Nominal Uang</label>
							</div>
							<small>Min : <span class="min-value-label">0,00</span> Max : <span class="max-value-label">0,00</span></small>
							<button type="button" class="btn btn-outline-success waves-effect pull-right" id="btnAddTran">Tambah</button>
						</div>
					</div>
				</form>
				<small>Note : Mohon lebih teliti untuk memasukkan nominal ke simpanan.</small>
				<table id="tableTransaction" class="table table-borderless" cellspacing="0" width="100%">
					<thead class="bg-info z-depth-1-half text-center text-white">
					<th>Simpanan</th>
					<th>Dana</th>
					</thead>
				</table>
			</div>
			<div class="modal-footer justify-content-end">
				<button type="button" class="btn btn-outline-danger waves-effect" id="btnNo">Tidak</button>
				<button type="button" class="btn btn-outline-success waves-effect" id="btnYes">Ya</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalJoinSaving" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="true" data-tranType="in">
	<div class="modal-dialog modal-notify modal-lg modal-info" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<p class="heading lead">Bergabung Simpanan Baru</p>
			</div>

			<div class="modal-body">
				<form id="formJoinSaving">
					<div class="row">
						<div class="col-12">
							<?= $this->load->view('component/dropdown-select2', array(
								"id" => "new-type-saving",
								"label" => "Jenis Simpanan",
								"validation" => array("required"),
								"placeholder" => true,
								"options" => array(),
							), true); ?>
						</div>
					</div>
					<p class="label-description-saving"></p>
				</form>
			</div>
			<div class="modal-footer justify-content-end">
				<button type="button" class="btn btn-outline-danger waves-effect" id="btnNo">Tidak</button>
				<button type="button" class="btn btn-outline-success waves-effect" id="btnYes">Ya</button>
			</div>
		</div>
	</div>
</div>
