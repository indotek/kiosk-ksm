<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<div class="limiter">
	<div class="container-login100">
		<div class="wrap-login100">
			<button class="btn btn-default waves-effect waves-light" type="button" id="btnCloseApp"
					style="position: absolute;top: 0px;right: 0px;">
				<i class="fas fa-times"></i>
			</button>
			<div class="login100-pic js-tilt" data-tilt>
				<img src="<?= $assets . 'images/img-01.png'; ?>" alt="IMG">
			</div>

			<form class="login100-form validate-form" autocomplete="off" id="form-login" method="post" action="">
				<span class="login100-form-title">
                    Staff Login
                </span>

				<div class="wrap-input100 validate-input">
					<input class="input100" type="text" name="email" id="email" placeholder="Email">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
                        <i class="fa fa-envelope" aria-hidden="true"></i>
                    </span>
				</div>

				<div class="wrap-input100 validate-input">
					<input class="input100" type="password" name="password" id="password" placeholder="Password">
					<span class="focus-input100"></span>
					<span class="symbol-input100">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                    </span>
				</div>

				<div class="container-login100-form-btn">
					<button class="login100-form-btn" type="button" id="btnLogin">
						Login
					</button>
				</div>

				<div class="text-center p-t-12">

				</div>

				<div class="text-center p-t-136">

				</div>
			</form>
		</div>
	</div>
</div>
