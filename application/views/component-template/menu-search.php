<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form class="search-form" role="search">
	<div class="md-form mt-0 waves-light">
		<input type="text" class="form-control py-2 search-menu" placeholder="Search">
	</div>
</form>
