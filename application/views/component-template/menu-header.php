<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<nav class="navbar fixed-top navbar-expand-lg scrolling-navbar double-nav">
	<div class="float-left">
		<a href="#" data-activates="slide-out" class="button-collapse"><i class="fas fa-bars"></i></a>
	</div>
	<div class="breadcrumb-dn mr-auto">
		<p><?= $this->config->item(TITLE_MENU); ?></p>
	</div>
	<div class="d-none change-mode">
		<ul class="nav navbar-nav nav-flex-icons ml-auto">
			<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle waves-effect" id="navbarLanguage" data-toggle="dropdown"
				   aria-haspopup="true" aria-expanded="false">
					<span
						class="flag-icon flag-icon-<?= get_code_lang($this->session->userdata('language')); ?>"></span>
					<span class="d-none d-md-inline-block"><?= ucfirst($this->session->userdata('language')); ?></span>
				</a>
				<div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarLanguage">
					<?php
					foreach (list_lang() as $key => $value) {
						if ($this->session->userdata('language') == $value) {
							?>
							<a class="dropdown-item active text-white" href="#">
								<span class="flag-icon flag-icon-<?= get_code_lang($value); ?>"></span>
								<span><?= ucfirst($value); ?></span>
							</a>
							<?php
						}
					}
					foreach (list_lang() as $key => $value) {
						if ($this->session->userdata('language') != $value) {
							?>
							<a class="dropdown-item" href="#" data-country-code="<?= get_code_lang($value); ?>">
								<span class="flag-icon flag-icon-<?= get_code_lang($value); ?>"></span>
								<span><?= ucfirst($value); ?></span>
							</a>
							<?php
						}
					}
					?>
				</div>
			</li>
		</ul>
	</div>
</nav>
