<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$menu = lang("menu");
?>

<ul class="collapsible collapsible-accordion sidebar-menu">
	<?php
	foreach ($menu as $menuKey => $menuValue) {
		if ($menuValue["type"] == "header") {
			?>
			<li>
				<span class="title-header"><?= $menuValue["title"]; ?></span>
			</li>
			<?php
		} else if ($menuValue["type"] == "dropdown") {
			?>
			<li class="menu-item-dropdown">
				<a class="collapsible-header waves-effect arrow-r">
					<?= (isset($menuValue["icon"])) ? '<i class="w-fa ' . $menuValue["icon"] . '"></i>' : ''; ?>
					<span class="title-menu can-search"><?= $menuValue["title"] ?></span>
					<i class="fas fa-angle-down rotate-icon"></i>
				</a>
				<div class="collapsible-body">
					<ul>
						<?php
						if (isset($menuValue["submenu"]) && is_array($menuValue["submenu"]) && count($menuValue["submenu"])) {
							foreach ($menuValue["submenu"] as $keySubmenu => $valueSubmenu) {
								?>
								<li>
									<a <?= (isset($menuValue["id"]) ? 'id="' . $menuValue["id"] . '"' : "") ?>
										data-redirect="<?= (isset($menuValue["target"])) ? site_url('pages/' . $valueSubmenu["target"]) : ''; ?>"
										class="collapsible-header waves-effect">
										<span class="title-menu can-search"><?= $valueSubmenu["title"] ?></span>
									</a>
								</li>
								<?php
							}
						}
						?>
					</ul>
				</div>
			</li>
			<?php
		} else if ($menuValue["type"] == "link") {
			?>
			<li>
				<a <?= (isset($menuValue["id"]) ? 'id="' . $menuValue["id"] . '"' : "") ?>
					<?= (isset($menuValue["target"])) ? 'data-redirect="'.site_url('pages/' . $menuValue["target"]).'"' : ''; ?>
					class="collapsible-header waves-effect">
					<?= (isset($menuValue["icon"])) ? '<i class="w-fa ' . $menuValue["icon"] . '"></i>' : ''; ?>
					<span class="title-menu can-search"><?= $menuValue["title"] ?></span>
				</a>
			</li>
			<?php
		}
	}
	?>
</ul>
