<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 12/06/2020
 * Time: 00:40
 */

$lang_template = lang("template-search-customer");
$lang_popup = lang("popup");
?>
<script>
	var langTemplateSearchCustomer = <?= json_encode($lang_template)?>;
</script>
<section id="sectionSearchCustomer">
	<div class="card card-cascade">
		<div class="view view-cascade gradient-card-header peach-gradient py-2 mx-4 mb-3">
			<h2 class="card-header-title mb-2"><?= $lang_template["title"]; ?></h2>
		</div>
		<div class="card-body card-body-cascade">
			<form id="formSearchCustomer" class="mb-0 text-center">
				<div class="md-form mb-3 mt-3">
					<input type="text" id="input-nik-search" class="form-control inputmask" name="input-nik-search"
						   data-inputmask="'mask': '9', 'repeat': 21"
						   minlength="10"
						   maxlength="20">
					<label for="input-nik-search" class=""><?= $lang_template["input-nik"]; ?></label>
				</div>
				<button type="button" id="btnSearchCustomer"
						class="btn btn-outline-secondary waves-effect btn-rounded m-0 w-25">
					<i class="fas fa-search mr-2"></i> <?= $lang_template["btnSearchCustomer"]; ?>
				</button>
			</form>
		</div>
	</div>
	<div class="modal fade" id="modalSearchCustomer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		 aria-hidden="true">
		<div class="modal-dialog modal-notify modal-danger" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<p class="heading lead modal-title"></p>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true" class="white-text">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="text-center">
						<i class="fas fa-times fa-4x mb-3 animated rotateIn modal-icon"></i>
						<p id="modal-message"></p>
					</div>
				</div>
				<div class="modal-footer justify-content-end">
					<button type="button" class="btn btn-outline-danger waves-effect"
							data-dismiss="modal"><?= $lang_popup["button"]["btnNo"]; ?></button>
					<button type="button" class="btn btn-outline-success waves-effect"
							id="btnConfirmYes"><?= $lang_popup["button"]["btnYes"]; ?></button>
				</div>
			</div>
		</div>
	</div>
</section>
