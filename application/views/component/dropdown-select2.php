<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 11:46
 */

defined('BASEPATH') OR exit('No direct script access allowed');


/**
 * Parameter
 *
 * "id": "",
 * "name": "",
 * "label": "",
 * "attribute": "",
 * "attribute": ["value"],
 * "attribute": [{"key": "", "value": ""}],
 * "validation": "",
 * "validation": ["value"],
 * "validation": [{"key": "", "value": ""}],
 * "placeholder": false,
 * "options": [{"key": "", "value": "", "selected": false}]
 */
?>
<?php
$gLabel = lang("form");
?>

<div class="md-form">
	<select class="browser-default select2"
		<?php
		if (!isset($attribute)) {
			$attribute = array();
		}
		$format = "select-";
		$id = $format . $id;
		array_push($attribute, array("key" => "id", "value" => $id));
		if (isset($name)) {
			$name = $format . $name;
			array_push($attribute, array("key" => "name", "value" => $name));
		} else {
			array_push($attribute, array("key" => "name", "value" => $id));
		}

		if (isset($validation)) {
			$attribute = array_merge($attribute, $validation);
		}

		if (isset($placeholder) && $placeholder) {
			if (isset($label) && is_string($label) && strlen($label) > 0) {
				array_push($attribute, array("key" => "placeholder", "value" => $gLabel["select"]["placeholder"] . ' ' . $label));
			} else if (isset($tab_lang[$id])) {
				array_push($attribute, array("key" => "placeholder", "value" => $gLabel["select"]["placeholder"] . ' ' . $tab_lang[$id]));
			}
		}
		if (isset($attribute)) {
			if (is_array($attribute)) {
				$array_attribute = array();
				foreach ($attribute as $key => $value) {
					if (is_array($value)) {
						$item_attribute = array();
						if (isset($value["key"])) {
							array_push($item_attribute, $value["key"]);
						}
						if (isset($value["value"])) {
							array_push($item_attribute, '"' . $value["value"] . '"');
						}
						array_push($array_attribute, join("=", $item_attribute));
					} else {
						array_push($array_attribute, $value);
					}
				}
				echo join(" ", $array_attribute);
			} else echo $attribute;
		}
		?>
	>
		<?php
		if (!isset($options)) {
			$options = array();
			for ($i = 0; $i < 500; $i++) {
				$item_option = array(
					"value" => $i,
					"label" => "Sample " . $i,
				);
				array_push($options, $item_option);
			}
		}

		if (is_array($options)) {
			foreach ($options as $key => $value) {
				echo '<option value="' . $value["value"] . '" ' . ((isset($value["selected"]) && $value["selected"]) ? 'selected' : '') . '>' . $value["label"] . '</option>';
			}
		}
		?>
	</select>
	<label for="select-<?= $id; ?>" class="active"><?php
		if (isset($label) && is_string($label) && strlen($label) > 0) {
			echo $label;
		} else if (isset($tab_lang[$id])) {
			echo $tab_lang[$id];
		}
		?></label>
</div>
