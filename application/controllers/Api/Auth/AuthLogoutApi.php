<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthLogoutApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
//		$this->load->model("master_model", "masterModel");

//		if(is_have_not_yet_to_sync()) {
//			$this->responseFailed(API_ERROR_LOGOUT_SYNC);
//		}

		$this->session->unset_userdata(SESSION_USER);

		$this->responseSuccess();
	}
}
