<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthLoginApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$this->load->model("user_model", "userModel");

		$email = $this->post("email");
		$password = $this->post("password");

		$password = UserPasswordEncrypt($password);

		if (!$email || !$password) $this->responseFailed(API_ERROR_PARAMETER_REQUIRED);

		$userData = $this->userModel->check_users_login($email, $password);

		if ($userData == NULL) $this->responseFailed(API_ERROR_DATA_NOT_FOUND);

		$this->session->set_userdata(SESSION_USER, $userData);

		$this->responseSuccess($userData);
	}
}
