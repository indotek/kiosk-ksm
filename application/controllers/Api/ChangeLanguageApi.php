<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ChangeLanguageApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$code = $this->post("code");

		$lang = get_label_lang($code);

		$this->session->set_userdata("language", $lang);

		$this->responseSuccess(array(
			"code" => $code,
			"language" => $lang,
		));
	}
}
