<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MasterDistrictListApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$this->db->where('cities_id', $this->post("regencyId"));

		$this->db->where('status', 1);

		$data = $this->db->get("m_addr_districts")->result_array();

		$this->responseSuccess($data);
	}
}
