<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MasterVillageListApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$this->db->where('district_id', $this->post("districtId"));

		$this->db->where('status', 1);

		$data = $this->db->get("m_addr_villages")->result_array();

		$this->responseSuccess($data);
	}
}
