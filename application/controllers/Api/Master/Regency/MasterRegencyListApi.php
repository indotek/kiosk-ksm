<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MasterRegencyListApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$this->db->where('provinces_id', $this->post("provinceId"));

		$this->db->where('status', 1);

		$data = $this->db->get("m_addr_cities")->result_array();

		$this->responseSuccess($data);
	}
}
