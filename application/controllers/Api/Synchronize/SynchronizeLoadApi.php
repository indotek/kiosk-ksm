<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SynchronizeLoadApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$dateNow = date('Y-m-d H:i:s');
		$log_sync = log_sync_history(MODULE_SYNCHRONIZE_ALL, $dateNow, "", "", "");

		if (!check_connection_database()) {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 0;
			$log_sync["message"] = "No connection to cloud database";
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseFailed(API_ERROR_SYNCHRONIZE_CONNECTION);
		}

		$this->load->model("customer_model", "customerModel");
		$this->load->model("customerProperty_model", "customerPropertyModel");
		$this->load->model("customerAccount_model", "customerAccountModel");
		$this->load->model("customerTransaction_model", "customerTransactionModel");
		$this->load->model("CustomerTransactionDetail_model", "customerTransactionDetailModel");

		$this->load->model("role_model", "roleModel");
		$this->load->model("user_model", "userModel");
		$this->load->model("master_model", "masterModel");

		$data_sync = array();
		$is_sync_success = array();
		$is_sync_failed = array();

		$syncRole = $this->roleModel->sync($log_sync);
		$data_sync["role"] = $syncRole;
		if ($syncRole["status"] != 0) array_push($is_sync_success, "role");
		else array_push($is_sync_failed, "role");

		$syncUser = $this->userModel->sync($log_sync);
		$data_sync["user"] = $syncUser;
		if ($syncUser["status"] != 0) array_push($is_sync_success, "user");
		else array_push($is_sync_failed, "user");

		$syncMaster = $this->masterModel->sync($log_sync);
		$data_sync["master"] = $syncMaster;
		if ($syncMaster["status"] != 0) array_push($is_sync_success, "master");
		else array_push($is_sync_failed, "master");

		$syncCustomer = $this->customerModel->sync($log_sync);
		$data_sync["customer"] = ($syncCustomer["status"] != 0);
		if ($syncCustomer["status"] != 0) array_push($is_sync_success, "customer");
		else array_push($is_sync_failed, "customer");

		$syncCustomerProperty = $this->customerPropertyModel->sync($log_sync);
		$data_sync["customer_property"] = ($syncCustomerProperty["status"] != 0);
		if ($syncCustomerProperty["status"] != 0) array_push($is_sync_success, "customer_property");
		else array_push($is_sync_failed, "customer_property");

		$syncCustomerAccount = $this->customerAccountModel->sync($log_sync);
		$data_sync["customer_account"] = ($syncCustomerAccount["status"] != 0);
		if ($syncCustomerAccount["status"] != 0) array_push($is_sync_success, "customer_account");
		else array_push($is_sync_failed, "customer_account");

		$syncCustomerTransaction = $this->customerTransactionModel->sync($log_sync);
		$data_sync["customer_transaction"] = ($syncCustomerTransaction["status"] != 0);
		if ($syncCustomerTransaction["status"] != 0) array_push($is_sync_success, "customer_transaction");
		else array_push($is_sync_failed, "customer_transaction");

		$syncCustomerTransactionDetail = $this->customerTransactionDetailModel->sync($log_sync);
		$data_sync["customer_transaction_detail"] = ($syncCustomerTransactionDetail["status"] != 0);
		if ($syncCustomerTransactionDetail["status"] != 0) array_push($is_sync_success, "customer_transaction_detail");
		else array_push($is_sync_failed, "customer_transaction_detail");

		if (count($is_sync_success) == 0) {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 0;
			$log_sync["message"] = "Nothing to synchronize";
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseFailed(API_ERROR_SYNCHRONIZE);
		} else {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 1;
			$log_sync["message"] = "Success to synchronize";
			$log_sync["total_sync"] =
				$syncRole["length_data"]["success"] +
				$syncUser["length_data"]["success"] +
				$syncMaster["all_length_data"] +
				$syncCustomer["length_data"] +
				$syncCustomerProperty["length_data"] +
				$syncCustomerAccount["length_data"] +
				$syncCustomerTransaction["length_data"] +
				$syncCustomerTransactionDetail["length_data"]
			;
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseSuccess($data_sync);
		}
	}
}
