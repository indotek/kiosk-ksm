<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SynchronizeMasterPreloadApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		if(check_log_sync_history_is_now(MODULE_SYNCHRONIZE_MASTER)) {
			$this->responseFailed(API_ERROR_SYNCHRONIZE_NOT_NEED);
		}

		$dateNow = date('Y-m-d H:i:s');
		$log_sync = log_sync_history(MODULE_SYNCHRONIZE_MASTER, $dateNow, "", "", "");

		if (!check_connection_database()) {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 0;
			$log_sync["message"] = "No connection to cloud database";
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseFailed(API_ERROR_SYNCHRONIZE_CONNECTION);
		}

		$this->load->model("role_model", "roleModel");
		$this->load->model("user_model", "userModel");
		$this->load->model("master_model", "masterModel");

		$data_sync = array();
		$is_sync_success = array();
		$is_sync_failed = array();

		$syncRole = $this->roleModel->sync($log_sync);
		$data_sync["role"] = $syncRole;
		if ($syncRole["status"] != 0) array_push($is_sync_success, "role");
		else array_push($is_sync_failed, "role");

		$syncUser = $this->userModel->sync($log_sync);
		$data_sync["user"] = $syncUser;
		if ($syncUser["status"] != 0) array_push($is_sync_success, "user");
		else array_push($is_sync_failed, "user");

		$syncMaster = $this->masterModel->sync($log_sync);
		$data_sync["master"] = $syncMaster;
		if ($syncMaster["status"] != 0) array_push($is_sync_success, "master");
		else array_push($is_sync_failed, "master");

		if (count($is_sync_success) == 0) {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 0;
			$log_sync["message"] = "Nothing to synchronize";
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseFailed(API_ERROR_SYNCHRONIZE);
		} else {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 1;
			$log_sync["message"] = "Success to synchronize";
			$log_sync["total_sync"] =
				$syncRole["length_data"]["success"] +
				$syncUser["length_data"]["success"] +
				$syncMaster["all_length_data"]
			;
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseSuccess($data_sync);
		}
	}
}
