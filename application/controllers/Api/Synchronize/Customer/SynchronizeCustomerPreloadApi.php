<?php
defined('BASEPATH') or exit('No direct script access allowed');

class SynchronizeCustomerPreloadApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		if(check_log_sync_history_is_now(MODULE_SYNCHRONIZE_CUSTOMER)) {
			$this->responseFailed(API_ERROR_SYNCHRONIZE_NOT_NEED);
		}

		$dateNow = date('Y-m-d H:i:s');
		$log_sync = log_sync_history(MODULE_SYNCHRONIZE_CUSTOMER, $dateNow, "", "", "");

		if (!check_connection_database()) {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 0;
			$log_sync["message"] = "No connection to cloud database";
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseFailed(API_ERROR_SYNCHRONIZE_CONNECTION);
		}

		$this->load->model("customer_model", "customerModel");
		$this->load->model("customerProperty_model", "customerPropertyModel");
		$this->load->model("customerAccount_model", "customerAccountModel");

		$data_sync = array();
		$is_sync_success = array();
		$is_sync_failed = array();

		$syncCustomer = $this->customerModel->sync($log_sync);
		$data_sync["customer"] = ($syncCustomer["status"] != 0);
		if ($syncCustomer["status"] != 0) array_push($is_sync_success, "customer");
		else array_push($is_sync_failed, "customer");

		$syncCustomerProperty = $this->customerPropertyModel->sync($log_sync);
		$data_sync["customer_property"] = ($syncCustomerProperty["status"] != 0);
		if ($syncCustomerProperty["status"] != 0) array_push($is_sync_success, "customer_property");
		else array_push($is_sync_failed, "customer_property");

		$syncCustomerAccount = $this->customerAccountModel->sync($log_sync);
		$data_sync["customer_account"] = ($syncCustomerAccount["status"] != 0);
		if ($syncCustomerAccount["status"] != 0) array_push($is_sync_success, "customer_account");
		else array_push($is_sync_failed, "customer_account");

		if (count($is_sync_success) == 0) {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 0;
			$log_sync["message"] = "Nothing to synchronize";
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseFailed(API_ERROR_SYNCHRONIZE);
		} else {
			$log_sync["end_sync"] = date('Y-m-d H:i:s');
			$log_sync["status"] = 1;
			$log_sync["message"] = "Success to synchronize";
			$log_sync["total_sync"] =
				$syncCustomer["length_data"] +
				$syncCustomerProperty["length_data"] +
				$syncCustomerAccount["length_data"]
			;
			log_sync_history_update($log_sync["id"], $log_sync);
			$this->responseSuccess($data_sync);
		}
	}
}
