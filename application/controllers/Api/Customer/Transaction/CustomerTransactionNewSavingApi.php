<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerTransactionNewSavingApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$dateNow = date("Y-m-d H:i:s");
		$this->load->model("customer_model", "customerModel");

		$data = $this->post();
		$member_id = $data["member_id"];
		$savings_products_id = $data["saving_product"];

		$this->customerModel->join_new_saving($dateNow, $member_id, $savings_products_id, 0);

		$this->responseSuccess();
	}
}
