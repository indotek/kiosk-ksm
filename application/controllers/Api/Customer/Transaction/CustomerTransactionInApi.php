<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerTransactionInApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$dateNow = date("Y-m-d H:i:s");
		$this->load->model("customer_model", "customerModel");

		$data = $this->post();

		$transaction = array();

		if(isset($data["transaction"])) {
			$transaction = $data["transaction"];
		}

		if(count($transaction) > 0) {
			$sample = $transaction[0];
			$member_id = $sample["member_id"];
			$this->db->trans_begin();
			$this->customerModel->transaction_in($dateNow, $member_id, $transaction);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
			} else {
				$this->db->trans_commit();
			}
		}

		$this->responseSuccess();
	}
}
