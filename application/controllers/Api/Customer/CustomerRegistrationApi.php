<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerRegistrationApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$this->load->model("customer_model", "customerModel");

		$data = $this->post();

		if (isset($data["personalData"]["homeNumber"])) $data["personalData"]["homeNumber"] = preg_replace("/\-|\_/", "", $data["personalData"]["homeNumber"]);

		if (isset($data["personalData"]["phoneNumber"])) $data["personalData"]["phoneNumber"] = preg_replace("/\-|\_/", "", $data["personalData"]["homeNumber"]);


		$this->db->where("nik", $data["personalData"]["nik"]);
		$memberIsExist = $this->db->get("m_members")->row_array();

		if($memberIsExist != null) {
			$this->responseFailed(API_ERROR_DATA_IS_EXIST);
		}

		$isSuccess = $this->customerModel->register($data);

		if ($isSuccess) {
			$where = array();
			$whereAccount = array();

			if($data["personalData"]["nik"] != null && strlen($data["personalData"]["nik"]) > 0) {
				$where["nik"] = $data["personalData"]["nik"];
				$whereAccount["member_id"] = $data["personalData"]["nik"];
			}

			$this->db->where($where);
			$memberIsExist = $this->db->get("m_members")->row_array();

			$dataResponse = array();

			if($memberIsExist != null) {
				$dataResponse["customer"] = $memberIsExist;
				$this->db->where($whereAccount);
				$memberAccounts = $this->db->get("m_savings_accounts")->result_array();

				for ($i = 0; $i < count($memberAccounts); $i++) {
					$rowMemberAccount = $memberAccounts[$i];

					$this->db->where("id", $rowMemberAccount["savings_products_id"]);
					$savingProduct = $this->db->get("m_savings_products")->row_array();
					$memberAccounts[$i]["saving_product"] = $savingProduct;
				}

				$dataResponse["customer"] = $memberAccounts;
			}

			$this->responseSuccess($dataResponse);
		} else {
			$this->responseFailed(API_ERROR_QUERY_INSERT);
		}
	}
}
