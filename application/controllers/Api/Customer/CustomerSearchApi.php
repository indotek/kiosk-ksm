<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerSearchApi extends MY_Controller_Api
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_post()
	{
		$this->load->model("customer_model", "customerModel");

		$data = $this->post();

		$where = array();
		$whereAccount = array();

		if($data["nik"] != null && strlen($data["nik"]) > 0) {
			$where["nik"] = $data["nik"];
			$whereAccount["member_id"] = $data["nik"];
		}

		if(count($where) == 0) $this->responseFailed(API_ERROR_NOT_HAS_PARAM);

		$this->db->where($where);
		$memberIsExist = $this->db->get("m_members")->row_array();

		if($memberIsExist == null) {
			$this->responseFailed(API_ERROR_DATA_NOT_FOUND);
		}else{
			$this->db->where($whereAccount);
			$memberAccounts = $this->db->get("m_savings_accounts")->result_array();

			for ($i = 0; $i < count($memberAccounts); $i++) {
				$rowMemberAccount = $memberAccounts[$i];

				$this->db->where("id", $rowMemberAccount["savings_products_id"]);
				$savingProduct = $this->db->get("m_savings_products")->row_array();
				$memberAccounts[$i]["saving_product"] = $savingProduct;
			}

			$this->db->where($whereAccount);
			$memberTransaction = $this->db->get("m_saving_transaction")->result_array();

			for ($i = 0; $i < count($memberTransaction); $i++) {
				$rowMemberTransaction = $memberTransaction[$i];

				$this->db->where("member_id", $data["nik"]);
				$this->db->where("created", $rowMemberTransaction["created"]);
				$memberTransactionDetail = $this->db->get("m_saving_transaction_detail")->result_array();
				$memberTransaction[$i]["detail"] = $memberTransactionDetail;
			}

			$dataResponse = array(
				"customer" => $memberIsExist,
				"customerAccounts" => $memberAccounts,
				"memberTransaction" => $memberTransaction
			);
			$this->responseSuccess($dataResponse);
		}
	}
}
