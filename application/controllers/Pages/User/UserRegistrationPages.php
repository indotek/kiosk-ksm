<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserRegistrationPages extends MY_Controller_Pages
{

	public function __construct()
	{
		$this->isNeedLogin = true;
		$this->title = "Registration User";
		parent::__construct();
	}

	public function index()
	{
		$this->css = array();

		$this->js = array();

		$this->view('pages/user/registration');
	}
}
