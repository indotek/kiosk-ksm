<?php
defined('BASEPATH') or exit('No direct script access allowed');

class AuthPages extends MY_Controller_Pages
{
	public function __construct()
	{
		$this->isNeedLogin = false;
		$this->title = "Login";
		$this->index_template = "blank-template";
		parent::__construct();
	}

	public function index()
	{
		$this->css = array(
			'module/login/login-utils.css',
			'module/login/login.css',
		);

		$this->js = array(
			'module/login/login.js',
		);
		$this->view('pages/auth/login');
	}
}
