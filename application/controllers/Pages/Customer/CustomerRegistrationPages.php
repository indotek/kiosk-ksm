<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerRegistrationPages extends MY_Controller_Pages
{

	public function __construct()
	{
		$this->isNeedLogin = true;
		parent::__construct();
	}

	public function index()
	{
		$this->lang->load('customer_registration', $this->language);
		$this->title = lang("tab_title");

		$this->css = array(
			"module/customer/registration/main.css",
		);

		$this->js = array(
			"module/customer/registration/main.js",
		);

		$this->db->where("id", 1);
		$savingProductPokok = $this->db->get("m_savings_products")->row_array();

		$this->db->where("id", 2);
		$savingProductWajib = $this->db->get("m_savings_products")->row_array();

		$this->view_data = array(
			"savingProducts" => array(
				"pokok" => $savingProductPokok,
				"wajib" => $savingProductWajib,
			)
		);

		$this->view('pages/customer/registration');
	}
}
