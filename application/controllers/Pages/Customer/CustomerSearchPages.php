<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CustomerSearchPages extends MY_Controller_Pages
{

	public function __construct()
	{
		$this->isNeedLogin = true;
		parent::__construct();
	}

	public function index()
	{
		$this->lang->load('customer_search', $this->language);
		$this->title = lang("tab_title");

		$this->css = array(
			"module/customer/search/main.css",
		);

		$this->js = array(
			"module/customer/search/main.js",
			"module/customer/customer-search.js",
		);

		$this->db->where("status", 1);
		$savingProduct = $this->db->get("m_savings_products")->result_array();

		$this->view_data = array(
			"savingProducts" => $savingProduct
		);

		$this->view('pages/customer/search');
	}
}
