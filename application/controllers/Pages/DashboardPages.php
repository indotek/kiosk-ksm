<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DashboardPages extends MY_Controller_Pages
{

	public function __construct()
	{
		$this->isNeedLogin = true;
		$this->title = "Dashboard";
		parent::__construct();
	}

	public function index()
	{
		$this->css = array();

		$this->js = array(
			"module/dashboard/main.js",
			"module/customer/customer-search.js",
		);

		$this->view('pages/dashboard');
	}
}
