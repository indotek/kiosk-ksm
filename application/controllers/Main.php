<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Controller_Pages
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		redirect(site_url($this->config->item(REDIRECT_BEFORE_LOGIN)));
	}

	public function clear()
	{
		$this->session->sess_destroy();
		redirect(site_url($this->config->item(REDIRECT_BEFORE_LOGIN)));
	}
}
