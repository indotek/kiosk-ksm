<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 19:34
 */
class Errors extends MY_Controller_Pages
{

	public function __construct()
	{
//		$this->isNeedLogin = false;
		$this->title = "Error";
		$this->index_template = "blank-template";
		parent::__construct();
	}

	public function index()
	{
		$this->error_404();
	}

	public function error_404()
	{
		$this->output->set_status_header('404');

		$this->view_data = array(
			"heading" => "404 Page Not Found",
			"message" => "The page you requested was not found.",
		);

		$this->css = array();

		$this->js = array();

		$this->view('errors/html/error_404');
	}
}
