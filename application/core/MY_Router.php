<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 07/04/2020
 * Time: 15:11
 */
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Router extends CI_Router
{
	public function __construct(array $routing = NULL)
	{
		parent::__construct($routing);
	}

	public function mapDir($val)
	{
		return ucfirst($val);
	}

	protected function _validate_request($segments)
	{
		/*log_message('debug', 'Get Segments = '.json_encode($segments));
		$c = count($segments);
		$directory_override = isset($this->directory);

		for ($i = 0; $i < $c; $i++) {
			$s = ucfirst($segments[$i]);
			$t = $this->directory . $s;
			$p = APPPATH . 'controllers/' . $t;
			if (file_exists($p)) {
				if (is_dir($p)) {
					$this->set_directory($t);
					continue;
				}
			} else {
				$b = ucfirst($segments[$i - 1]);
				$t = $this->directory . $b . $s;
				$p = APPPATH . 'controllers/' . $t . EXT;
				if (file_exists($p)) {
					$segments[$i] = $b . $s;
				}
			}
		}

		$this->directory = "";
		while ($c-- > 0) {
			$test = $this->directory
				. ucfirst($this->translate_uri_dashes === TRUE ? str_replace('-', '_', $segments[0]) : $segments[0]);

			if (!file_exists(APPPATH . 'controllers/' . $test . '.php')
				&& $directory_override === FALSE
				&& is_dir(APPPATH . 'controllers/' . $this->directory . $segments[0])
			) {
				$this->set_directory(array_shift($segments), TRUE);
				continue;
			}

			return $segments;
		}

		// This means that all segments were actually directories
		return $segments;*/
		$c = count($segments);
		$directory_override = isset($this->directory);

		//set_directory($segments[0]);
		//$segments = array_slice($segments, 1);

		while (count($segments) > 0) {
			$ucSegment = ucfirst($segments[0]);
			$p = APPPATH . 'controllers/' . $this->directory . $ucSegment;
			if (file_exists($p . EXT)) {
				break;
			} elseif (file_exists($p)) {
				if (is_dir($p)) {
					$this->set_directory($this->directory . $ucSegment);
					$segments = array_slice($segments, 1);
				} else {
					break;
				}
			} else {
				$arrDir = explode("/", $this->fetch_directory());
				array_pop($arrDir);
				$countArrDir = count($arrDir);
				$groupDir = $arrDir[0];
				$strDir = $ucSegment . ucfirst($groupDir);
				if ($countArrDir > 1) {
					$arrDir = array_slice($arrDir, 1);
					$arrDir = array_map(array($this, 'mapDir'), $arrDir);
					$strDir = implode("", $arrDir) . $strDir;
				}
				$p = APPPATH . 'controllers/' . $this->directory . $strDir . EXT;
				if (file_exists($p)) {
					$segments[0] = $strDir;
				} else {
					break;
				}
			}
			// Set the directory and remove it from the segment array
		}

		if (count($segments) > 0) {
			$ucSegment = ucfirst($segments[0]);
			// Does the requested controller exist in the sub-folder?
			if (!file_exists(APPPATH . 'controllers/' . $this->fetch_directory() . $ucSegment . EXT)) {
				if (!empty($this->routes['404_override'])) {
					$x = explode('/', $this->routes['404_override']);

					$this->set_directory('');
					$this->set_class(ucfirst($x[0]));
					$this->set_method(isset($x[1]) ? $x[1] : 'index');

					return $x;
				} else {
					show_404($this->fetch_directory() . $ucSegment);
				}
			}
		} else {
			// Is the method being specified in the route?
			if (strpos($this->default_controller, '/') !== FALSE) {
				$x = explode('/', $this->default_controller);

				$this->set_class(ucfirst($x[0]));
				$this->set_method($x[1]);
			} else {
				$this->set_class(ucfirst($this->default_controller));
				$this->set_method('index');
			}

			// Does the default controller exist in the sub-folder?
			if (!file_exists(APPPATH . 'controllers/' . $this->fetch_directory() . $this->default_controller . '.php')) {
				$this->directory = '';
				return array();
			}

		}

		return $segments;
	}
}
