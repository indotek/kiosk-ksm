<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';

class MY_Controller_Api extends REST_Controller
{

	function __construct()
	{
		parent::__construct();

		$langApi = $this->head("X-Language-Api");
		if($langApi) {
			$this->config->set_item("language", $langApi);
			$this->lang->load('rest_controller', $langApi, FALSE, TRUE, __DIR__ . '/../');
		}
		log_message('debug', "Request data : " . json_encode($this->post()));
	}

	public function responseFailed($error, $data = null, $message = "")
	{
		$resp = $this->errorCode($error);
		log_message('debug', "Response data : " . json_encode($resp));
		if($message != "") $resp["message"] = $message;
		if ($data != null) $resp["data"] = $data;
		$this->response($resp, REST_Controller::HTTP_OK);
		exit(EXIT_SUCCESS);
	}

	public function errorCode($errorCode)
	{
		$langErrorMessage = lang("error_api");
		return array(
			"code" => $errorCode,
			"message" => $langErrorMessage[$errorCode],
		);
	}

	public function responseSuccess($data = null)
	{
		$resp = $this->errorCode(API_SUCCESS);
		if ($data != null) $resp["data"] = $data;
		log_message('debug', "Response data : " . json_encode($resp));
		$this->response($resp, REST_Controller::HTTP_OK);
		exit(EXIT_SUCCESS);
	}
}
