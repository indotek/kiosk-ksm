<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller_Pages extends CI_Controller
{

	var $index_template = "main-template";
	var $title = "";
	var $view_data = array();
	var $css = array();
	var $js = array();
	var $isNeedLogin = null;
	var $language = "";

	function __construct()
	{
		parent::__construct();
		$this->load->config("template");
		log_message('debug', "log debug");
		log_message('debug', "Is Need Login : " . ($this->isNeedLogin ? "true" : "false"));
		log_message('debug', "Is User Login : " . (UserIsLogged() ? "true" : "false"));
		if (!is_null($this->isNeedLogin)) {
			if ($this->isNeedLogin) {
				if (!UserIsLogged()) {
					redirect(site_url($this->config->item(REDIRECT_BEFORE_LOGIN)));
				}
			} else {
				if (UserIsLogged()) {
					redirect(site_url($this->config->item(REDIRECT_AFTER_LOGIN)));
				}
			}
		}
		$this->language = $this->config->item("language");
		if ($this->session->userdata('language')) $this->language = $this->session->userdata('language');
		else {
			$array = array(
				'language' => $this->language
			);
			$this->session->set_userdata($array);
		}
		if (!is_dir(APPPATH . 'language/' . $this->language)) {
			$this->language = $this->config->item("language");
			$array = array(
				'language' => $this->language
			);
			$this->session->set_userdata($array);
		}

		$this->lang->load("template", $this->language);
	}

	public function view($url = "", $is_text = FALSE)
	{

		$view = "";
		if ($url == "") {
			return false;
		}
		$template = array();
		if (isset($this->view_data) && count($this->view_data)) {
			$template = $this->view_data;
		}
		$template["title"] = $this->config->item(TITLE_WINDOW) . ($this->title != "" ? " - " . $this->title : "");
		$template["css"] = $this->config->item("css");
		$template["js"] = $this->config->item("js");
		$template["url"] = base_url();
		$template["assets"] = base_url("assets") . "/";
		if (count($this->css) > 0) {
			foreach ($this->css as $key => $value) {
				$template["css"][] = $value;
			}
		}
		if ($this->session->userdata('language') != "english") {
			$arrayJsLanguage = array(
				'jquery-validate/js/localization/messages_' . get_code_lang($this->session->userdata('language')) . '.js'
			);
			$template["js"]["after"] = insertToIndex($template["js"]["after"], 1, $arrayJsLanguage);
		}
		if (count($this->js)) {
			foreach ($this->js as $key => $value) {
				$template["js"]["after"][] = $value;
			}
		}
		if ($this->index_template == "") {
			$view = $url;
		} else {
			$template["content"] = $this->load->view($url, $template, TRUE);
			$view = $this->index_template;
		}

		$this->load->view($view, $template, $is_text);
	}
}
