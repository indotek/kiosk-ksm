<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
TITLE TEMPLATE
*/
$config[TITLE_WINDOW] = "Kiosk KSM";
$config[TITLE_MENU] = "Koperasi Sido Mandiri";
$config[SHORT_TITLE_MENU] = "KSM";

$config["css"] = array(
	"fontawesome/css/pro.css",
	"bootstrap/css/bootstrap.css",
	"mdb/css/mdb.css",
	"material-icons/css/material-icons.css",
	"flag-icon/css/flag-icon.css",
	"select2/css/select2.css",
	"select2/css/select2-bootstrap.css",
	"select2/css/select2-material.css",
	"mdb/css/addons/datatables.css",
	"mdb/css/addons/datatables-select.css",
	"module/main.css",
);

$config["js"] = array(
	"before" => array(
		"jquery/js/jquery.js",
		"popper-js/js/popper.js",
		"bootstrap/js/bootstrap.js",
		"jquery-validate/js/jquery.validate.js",
		"jquery-validate/js/additional-methods.js",
		"tooltip/js/tooltip.js",
		"notify/js/bootstrap-notify.js",
		"jquery-inputmask/js/jquery.inputmask.js",
		"tilt/js/tilt.jquery.js",
		"select2/js/select2.full.js",
		"mdb/js/addons/datatables.js",
		"mdb/js/addons/datatables-select.js",
		"utils/string-utils.js",
		"utils/json.js",
		"utils/api.js",
		"utils/forms.js",
		"utils/lang-utils.js",
	),
	"after" => array(
		"mdb/js/mdb.js",
		"module/main.js",
	)
);

$config[REDIRECT_BEFORE_LOGIN] = 'pages/auth';
$config[REDIRECT_AFTER_LOGIN] = 'pages/dashboard';
