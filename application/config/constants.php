<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') or define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE') or define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') or define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') or define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') or define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ') or define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') or define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') or define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') or define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') or define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') or define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') or define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS') or define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') or define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') or define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') or define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') or define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') or define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') or define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') or define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') or define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') or define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


defined('EXT') or define('EXT', ".php");

defined('COMPANY') or define('COMPANY', "Koperasi Sido Mandiri");

defined('TIMEZONE_JAKARTA') or define('TIMEZONE_JAKARTA', "Asia/Jakarta");

defined('DATABASE_CLOUD_IP') or define('DATABASE_CLOUD_IP', "156.67.222.127");
defined('DATABASE_CLOUD_DBNAME') or define('DATABASE_CLOUD_DBNAME', "u577299784_ksm");
defined('DATABASE_CLOUD_USERNAME') or define('DATABASE_CLOUD_USERNAME', "u577299784_indotek");
defined('DATABASE_CLOUD_PASSWORD') or define('DATABASE_CLOUD_PASSWORD', "myindotek.com@123");

// ======== CONFIG TEMPLATE ============
defined('TITLE_WINDOW') or define('TITLE_WINDOW', "title_window");
defined('TITLE_MENU') or define('TITLE_MENU', "title_menu");
defined('SHORT_TITLE_MENU') or define('SHORT_TITLE_MENU', "short_title_menu");
defined('REDIRECT_BEFORE_LOGIN') or define('REDIRECT_BEFORE_LOGIN', "redirect_before_login");
defined('REDIRECT_AFTER_LOGIN') or define('REDIRECT_AFTER_LOGIN', "redirect_after_login");

// ======== SESSION ============
defined('SESSION_USER') or define('SESSION_USER', "user");
defined('SESSION_SIDEBAR') or define('SESSION_SIDEBAR', "sidebar_is_open");

// ======== API ERROR CODE ============
defined('API_SUCCESS') or define('API_SUCCESS', "0");
defined('API_ERROR_DATA_NOT_FOUND') or define('API_ERROR_DATA_NOT_FOUND', "-1000");
defined('API_ERROR_PARAMETER_REQUIRED') or define('API_ERROR_PARAMETER_REQUIRED', "-1001");
defined('API_ERROR_DATA_IS_EXIST') or define('API_ERROR_DATA_IS_EXIST', "-1002");
defined('API_ERROR_LOGOUT_SYNC') or define('API_ERROR_LOGOUT_SYNC', "-1003");
defined('API_ERROR_NOT_HAS_PARAM') or define('API_ERROR_NOT_HAS_PARAM', "-1004");


defined('API_ERROR_QUERY') or define('API_ERROR_QUERY', "-2000");
defined('API_ERROR_QUERY_INSERT') or define('API_ERROR_QUERY_INSERT', "-2001");
defined('API_ERROR_QUERY_UPDATE') or define('API_ERROR_QUERY_UPDATE', "-2002");
defined('API_ERROR_QUERY_DELETE') or define('API_ERROR_QUERY_DELETE', "-2003");

defined('API_ERROR_SYNCHRONIZE') or define('API_ERROR_SYNCHRONIZE', "-3001");
defined('API_ERROR_SYNCHRONIZE_CONNECTION') or define('API_ERROR_SYNCHRONIZE_CONNECTION', "-3002");
defined('API_ERROR_SYNCHRONIZE_NOT_NEED') or define('API_ERROR_SYNCHRONIZE_NOT_NEED', "-3003");


defined('MODULE_SYNCHRONIZE_MASTER') or define('MODULE_SYNCHRONIZE_MASTER', "MODULE_SYNCHRONIZE_MASTER");
defined('MODULE_SYNCHRONIZE_CUSTOMER') or define('MODULE_SYNCHRONIZE_CUSTOMER', "MODULE_SYNCHRONIZE_CUSTOMER");
defined('MODULE_SYNCHRONIZE_ALL') or define('MODULE_SYNCHRONIZE_ALL', "MODULE_SYNCHRONIZE_ALL");
