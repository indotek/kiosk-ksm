<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('asset_url')) {
	function asset_url($uri = '', $protocol = NULL)
	{
		return base_url("assets" . "/" . $uri, $protocol);
	}
}

if (!function_exists('get_previous_url')) {
	function get_previous_url()
	{
		if (isset($_SERVER['HTTP_REFERER'])) {
			return $_SERVER['HTTP_REFERER'];
		} else {
			return site_url();
		}
	}
}
