<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('UserIsLogged')) {
	function UserIsLogged()
	{
		return (get_instance()->session->has_userdata(SESSION_USER) ? true : false);
	}
}

if (!function_exists('UserLogged')) {
	function UserLogged()
	{
		return UserIsLogged() ? get_instance()->session->userdata(SESSION_USER) : new StdClass();
	}
}

if (!function_exists('GetKeyForEncryptionPassword')) {
	function GetKeyForEncryptionPassword()
	{
		$CI = get_instance();
		// $CI->load->config('config');
		$key = $CI->config->item('encryption_key');
		return $key;
	}
}

if (!function_exists('UserPasswordEncrypt')) {
	function UserPasswordEncrypt($string)
	{
		$key = GetKeyForEncryptionPassword();
		$result = '';
		for ($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key)) - 1, 1);
			$char = chr(ord($char) + ord($keychar));
			$result .= $char;
		}

		return base64_encode($result);
	}
}

if (!function_exists('UserPasswordDecrypt')) {
	function UserPasswordDecrypt($string)
	{
		$key = GetKeyForEncryptionPassword();
		$result = '';
		$string = base64_decode($string);

		for ($i = 0; $i < strlen($string); $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key)) - 1, 1);
			$char = chr(ord($char) - ord($keychar));
			$result .= $char;
		}
		return $result;
	}
}

// ------------------------------------------------------------------------

/* End of file Authenticate_helper.php */
/* Location: ./application/helpers/Authenticate_helper.php */
