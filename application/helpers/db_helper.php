<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('connect_db_hosting')) {
	function connect_db_hosting()
	{
		$CI = get_instance();
		return $CI->load->database('hosting', TRUE);
	}
}

if (!function_exists('get_ai_hosting')) {
	function get_ai_hosting($tableName)
	{
		$dbHosting = connect_db_hosting();
		$dbHosting->select("AUTO_INCREMENT");
		$dbHosting->where("TABLE_SCHEMA", DATABASE_CLOUD_DBNAME);
		$dbHosting->where("TABLE_NAME", $tableName);
		$autoIncrementHosting = $dbHosting->get("information_schema.TABLES")->row_array();

		return $autoIncrementHosting;
	}
}

if (!function_exists('update_ai_hosting')) {
	function update_ai_hosting($tableName, $value)
	{
		$dbHosting = connect_db_hosting();
		$dbHosting->query("ALTER TABLE ".DATABASE_CLOUD_DBNAME.".".$tableName." AUTO_INCREMENT = ".$value);
	}
}

if (!function_exists('get_ai_local')) {
	function get_ai_local($tableName)
	{
		$CI = get_instance();
		$CI->db->where("name", $tableName);
		$autoIncrementLocal = $CI->db->get("sqlite_sequence")->row_array();
		return $autoIncrementLocal;
	}
}

if (!function_exists('update_ai_local')) {
	function update_ai_local($tableName, $value)
	{
		$CI = get_instance();
		$CI->db->where("name", $tableName);
		$CI->db->update("sqlite_sequence", array("seq"=>$value));
	}
}

if (!function_exists('search_table_local_all')) {
	function search_table_local_all($col)
	{
		$tableNot = array("sqlite_sequence");
		if (!in_array($col, $tableNot)) {
			return $col;
		}
	}
}

if (!function_exists('search_table_local')) {
	function search_table_local($col)
	{
		$tableNot = array("sqlite_sequence", "users", "role", "m_members", "m_member_properties", "m_savings_accounts", "m_saving_transaction", "m_saving_transaction_detail");
		if (!in_array($col, $tableNot)) {
			return $col;
		}
	}
}

if (!function_exists('is_have_not_yet_to_sync')) {
	function is_have_not_yet_to_sync() {
		$CI = get_instance();
		$listTableLocal = $CI->db->list_tables();

		$listTableLocal = array_filter($listTableLocal, 'search_table_local_all');

		foreach ($listTableLocal as $key => $val) {
			$tableName = $val;

			$CI->db->where('is_sync', 0);
			$q = $CI->db->get($tableName);

			if($q->num_rows() > 0) {
				return true;
				break;
			}
		}

		return false;
	}
}

if (!function_exists('is_need_to_sync')) {
	function is_need_to_sync() {
		$CI = get_instance();

		$dbHosting = connect_db_hosting();

		$listTableHosting = $dbHosting->list_tables();
		$listTableLocal = $CI->db->list_tables();

		$listTableLocal = array_filter($listTableLocal, 'search_table_local_all');

		foreach ($listTableLocal as $key => $val) {
			$tableName = $val;

			if (in_array($tableName, $listTableHosting)) {
				$dbHosting->where('is_sync', 0);
				$q = $dbHosting->get($tableName);

				if($q->num_rows() > 0) {
					return true;
					break;
				}
			}
		}

		return false;
	}
}

if (!function_exists('log_sync_history')) {
	function log_sync_history($nameSync, $dateStart, $dateStop, $status, $message) {
		$dataHistory = array(
			"name_sync" => $nameSync,
			"status" => $status,
			"message" => $message,
			"total_sync" => 0,
			"start_sync" => $dateStart,
			"end_sync" => $dateStop,
		);
		$CI = get_instance();
		$CI->db->insert("t_history_sync", $dataHistory);

		$dataHistory["id"] = $CI->db->insert_id();

		return $dataHistory;
	}
}

if (!function_exists('log_sync_history_update')) {
	function log_sync_history_update($id, $data) {
		$CI = get_instance();
		$CI->db->where("id", $id);
		$CI->db->update("t_history_sync", $data);
	}
}

if (!function_exists('log_sync_history_detail')) {
	function log_sync_history_detail($tableName, $status, $dateSync, $countRow, $parentId) {
		$dataHistory = array(
			"name_table" => $tableName,
			"status" => $status,
			"created_date" => $dateSync,
			"count_row" => $countRow,
			"parent_id" => $parentId["id"]
		);
		$CI = get_instance();
		$CI->db->insert("t_history_sync_detail", $dataHistory);
	}
}

if (!function_exists('check_log_sync_history_is_now')) {
	function check_log_sync_history_is_now($nameSync) {
		$dateNow = date("Y-m-d") . " 00:00:00";

		$dataHistory = array(
			"name_sync" => $nameSync,
			"status" => 1
		);
		$CI = get_instance();
		$CI->db->order_by("start_sync", "desc");
		$CI->db->where($dataHistory);
		$q = $CI->db->get("t_history_sync")->row_array();

		$timeNow = strtotime($dateNow);
		$timeDb = strtotime(date("Y-m-d", strtotime($q["start_sync"])) . " 00:00:00");

		return $timeNow == $timeDb;
	}
}
