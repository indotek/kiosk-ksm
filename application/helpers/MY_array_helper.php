<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 18:20
 */

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('insertToIndex')) {
	function insertToIndex(array $array, $index = 0, array $data)
	{
		$array1 = array_slice($array, 0, $index, false);
		$array1 = array_merge($array1, $data);
		$array2 = array_slice($array, $index, count($array), false);
		$result = array_merge($array1, $array2);
		return $result;
	}
}

