<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('list_lang')) {
	function list_lang()
	{
		$CI = get_instance();
		$dir = directory_map(APPPATH . 'language', 1);

		$list_lang = array();
		foreach ($dir as $key => $value) {
			preg_match_all('/(\w+)/', $value, $regex);
			$value = (count($regex) > 0 ? (count($regex[0]) > 0 ? $regex[0][0] : $value) : $value);
			if (is_dir(APPPATH . 'language/' . $value)) {
				array_push($list_lang, $value);
			}
		}

		return $list_lang;
	}
}

if (!function_exists('get_code_lang')) {
	function get_code_lang($name)
	{
		$name = strtolower($name);
		$CI = get_instance();
		$CI->load->language("country");

		$country = lang("country");

		$keys = array_keys($country);
		$values = array_values($country);

		$map = function ($value) {
			return strtolower($value);
		};

		$values = array_map($map, $values);

		$i = array_search($name, $values);

		return strtolower($keys[$i]);
	}
}

if (!function_exists('get_label_lang')) {
	function get_label_lang($code)
	{
		$code = strtolower($code);
		$CI = get_instance();
		$CI->load->language("country");

		$country = lang("country");

		$keys = array_keys($country);
		$values = array_values($country);

		$map = function ($value) {
			return strtolower($value);
		};

		$keys = array_map($map, $keys);

		$i = array_search($code, $keys);

		return strtolower($values[$i]);
	}
}
