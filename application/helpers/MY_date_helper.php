<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('date_to_array_string')) {
	function date_to_array_string($date, $split_pattern)
	{
		return json_encode(explode($split_pattern, $date));
	}
}
