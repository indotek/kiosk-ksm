<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('get_options_data')) {
	function get_options_data($table, $value, $label, array $field = array())
	{
		$CI = get_instance();

		if(isset($field) && is_array($field) && count($field) > 0) {
			array_push($field, "status");
			$CI->db->select($field);
		}

		$CI->db->where('status', 1);

		$data = $CI->db->get($table)->result_array();

		$result = array();

		foreach ($data as $key => $col) {

			$item = array(
				"value" => $col[$value],
				"label" => $col[$label]
			);

			array_push($result, $item);
		}

		return $result;
	}
}
