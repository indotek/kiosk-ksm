<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('check_connection')) {
	function check_connection()
	{
		$host_name = 'www.google.com';
		$port_no = '80';

		$st = (bool)@fsockopen($host_name, $port_no, $err_no, $err_str, 10);
		if ($st) {
			return true;
		} else {
			return false;
		}
	}
}

if (!function_exists('check_connection_database')) {
	function check_connection_database()
	{
		$host_name = DATABASE_CLOUD_IP;
		$port_no = '80';

		$st = (bool)@fsockopen($host_name, $port_no, $err_no, $err_str, 10);
		if ($st) {
			return "true";
		} else {
			return "false";
		}
	}
}
