<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 08:02
 */

defined('BASEPATH') OR exit('No direct script access allowed');


$lang["menu"] = array(
	array(
		"type" => "link",
		"title" => "Dashboard",
		"icon" => "fas fa-house-user",
		"target" => "dashboard",
	),
	array(
		"type" => "header",
		"title" => "Customers",
	),
	array(
		"type" => "link",
		"title" => "Registration",
		"icon" => "fas fa-user-circle",
		"target" => "customer/registration"
	),
	array(
		"type" => "link",
		"title" => "Saving",
		"icon" => "fas fa-money-bill",
		"target" => "customer/search"
	),
	array(
		"type" => "header",
		"title" => "Preferences",
	),
//	array(
//		"type" => "link",
//		"title" => "Account",
//		"icon" => "fas fa-tachometer-alt",
//	),
	array(
		"type" => "link",
		"title" => "Logout",
		"icon" => "fas fa-sign-out-alt",
		"id" => "btnLogout"
	),
);

$lang["welcome"] = "<h2>Welcome {user},</h2><span>You login as {role} in Application Koperasi Sido Mandiri.</span>";


$lang["form"] = array(
	"select" => array(
		"placeholder" => "Choose"
	),
	"phoneID" => "Please enter a valid phone number",
	"nikNotRegistered" => "NIK is registered.",
	"datepicker" => array(
		"monthsFull" => array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'),
		"monthsShort" => array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'),
		"weekdaysFull" => array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'),
		"weekdaysShort" => array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'),
		"today" => 'Now',
		"clear" => 'Clear',
		"close" => 'Close',
		"format" => 'Choose: dddd, dd mmmm yyyy',
		"formatSubmit" => 'yyyy-mm-dd',
	)
);

$lang["popup"] = array(
	"logout" => array(
		"failed" => array(
			"title" => "Failed to logout.",
			"message" => "Some problems on service logout.",
		)
	),
	"synchronize" => array(
		"failed" => array(
			"message" => "Failed Synchronize"
		),
		"success" => array(
			"message" => "Success Synchronize"
		),
		"nothing_to_sync" => array(
			"message" => "Nothing data to Synchronize"
		),
	),
	"button" => array(
		"btnYes" => "Yes",
		"btnNo" => "No",
		"btnGagal" => "Cancel",
		"btnOk" => "OK",
		"btnConfirm" => "Confirm",
		"btnProcess" => "Process",
	)
);

$lang["template-search-customer"] = array(
	"title" => "Search Customer",
	"input-nik" => "Enter NIK Customer",
	"btnSearchCustomer" => "Search",
	"nik-empty" => "NIK is empty, can not find customer.",
	"nik-lessthan-minlength" => "NIK length less than %s, can not find customer.",
	"popupMessage-nik-already" => "Success found customer<br>Are you want to continue saving / credit ?",
	"popupMessage-nik-not-found" => "Failed found customer<br>Are you want to continue register customer ?",
	"popupTitle-nik-already" => "NIK is registered customer",
	"popupTitle-nik-not-found" => "NIK not registered customer",
);
