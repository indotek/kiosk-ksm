<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 08:01
 */

defined('BASEPATH') OR exit('No direct script access allowed');

$lang["tab_title"] = "Registration New Customer";
$lang["tab_form"] = array(
	array(
		"tab_id" => "personalData",
		"tab_title" => "Personal Data",
		"tab_no" => "A",
		"tab_template" => "personalData",
	),
	array(
		"tab_id" => "relationship",
		"tab_title" => "Relations Data",
		"tab_no" => "B",
		"tab_template" => "relationship",
	),
	array(
		"tab_id" => "ownerProperty",
		"tab_title" => "Owner Property",
		"tab_no" => "C",
		"tab_template" => "ownerProperty",
	),
	array(
		"tab_id" => "otherData",
		"tab_title" => "Others Data",
		"tab_no" => "D",
		"tab_template" => "otherData",
	),
);

$lang["form_personalData"] = array(
	"input-nik" => "Number Identity",
	"input-name" => "Full Name",
	"input-placeOfBirth" => "Place Of Birth",
	"input-dateOfBirth" => "Date Of Birth",
	"select-religion" => "Religion",
	"select-maritalStatus" => "Marital Status",
	"select-occupation" => "Occupation",
	"select-averageSalaryPerMonth" => "Average Monthly Salary",
	"textarea-address" => "Address (Format : Address Block Number)",
	"input-rt" => "RT",
	"input-rw" => "RW",
	"select-village" => "Village",
	"select-district" => "District",
	"select-regency" => "Regency",
	"select-province" => "Province",
	"input-homeNumber" => "Phone Home Number (Format : 0341-xxxx-xxxx)",
	"input-phoneNumber" => "Handphone Number (Format : 08xx-xxxx-xxxx)",
	"input-longStay" => "Long Stay",
	"textarea-otherAddress" => "Previous Address (Format : Address Block Number, Village District, Regency, Province)",
	"textarea-reasonOtherAddress" => "Reason Change Address",
);

$lang["form_relationship"] = array(
	"input-nikFamily" => "Number Identity",
	"input-nameFamily" => "Full Name",
	"select-relationFamily" => "Relation",
	"select-occupationFamily" => "Occupation",
	"select-averageSalaryPerMonthFamily" => "Average Monthly Salary",
	"input-amountDependent" => "Number of children still covered",
);

$lang["form_otherData"] = array(
	"textarea-otherData" => "Support Data / Another (Notes)",
);

$lang["notify_registration"] = array(
	"success" => array(
		"title" => "Success to Registration New Customer",
		"message" => "Customer can't use until doing synchronize to cloud."
	),
	"failed" => array(
		"title" => "Failed to Registration",
		"message" => "Has an error on data.",
	),
);
