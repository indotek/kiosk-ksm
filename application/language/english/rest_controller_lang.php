<?php

/*
 * English language
 */

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_rest_invalid_api_key'] = 'Invalid API key %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Invalid credentials';
$lang['text_rest_ip_denied'] = 'IP denied';
$lang['text_rest_ip_unauthorized'] = 'IP unauthorized';
$lang['text_rest_unauthorized'] = 'Unauthorized';
$lang['text_rest_ajax_only'] = 'Only AJAX requests are allowed';
$lang['text_rest_api_key_unauthorized'] = 'This API key does not have access to the requested controller';
$lang['text_rest_api_key_permissions'] = 'This API key does not have enough permissions';
$lang['text_rest_api_key_time_limit'] = 'This API key has reached the time limit for this method';
$lang['text_rest_ip_address_time_limit'] = 'This IP Address has reached the time limit for this method';
$lang['text_rest_unknown_method'] = 'Unknown method';
$lang['text_rest_unsupported'] = 'Unsupported protocol';

$lang["error_api"] = array(
	"0" => "SUCCESS",
	"-1000" => "DATA NOT FOUND",
	"-1001" => "SOME DATA IS REQUIRED",
	"-1002" => "DATA ALREADY EXIST",
	"-1003" => "YOU HAVE NOT BEEN SYNCHRONIZED.<br>PLEASE DO FIRST SYNCHRONIZATION TO REDUCE MISTAKES AND GET RECENT DATA.",
	"-1004" => "NOT HAVE PARAMETER",

	"-2001" => "FAILED TO INSERT DATA",
	"-3001" => "FAILED TO SYNCHRONIZE",
	"-3002" => "FAILED TO SYNCHRONIZE. NOTHING CONNECTION.",
	"-3003" => "NOTHING DATA NEED TO SYNCHRONIZE.",
);
