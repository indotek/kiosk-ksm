<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 08:01
 */

defined('BASEPATH') OR exit('No direct script access allowed');

$lang["tab_title"] = "Pendaftaran Nasabah Baru";
$lang["tab_form"] = array(
	array(
		"tab_id" => "personalData",
		"tab_title" => "Data Pribadi",
		"tab_no" => "A",
		"tab_template" => "personalData",
	),
	array(
		"tab_id" => "relationship",
		"tab_title" => "Data Keluarga",
		"tab_no" => "B",
		"tab_template" => "relationship",
	),
	array(
		"tab_id" => "ownerProperty",
		"tab_title" => "Data Kepemilikan / Kekayaan",
		"tab_no" => "C",
		"tab_template" => "ownerProperty",
	),
	array(
		"tab_id" => "otherData",
		"tab_title" => "Data Pendukung",
		"tab_no" => "D",
		"tab_template" => "otherData",
	),
);

$lang["form_personalData"] = array(
	"input-nik" => "Nomor KTP / Identitas Lain",
	"input-name" => "Nama Lengkap",
	"input-placeOfBirth" => "Tempat Lahir",
	"input-dateOfBirth" => "Tanggal Lahir",
	"select-religion" => "Agama",
	"select-maritalStatus" => "Status Perkawinan",
	"select-occupation" => "Pekerjaan / Usaha",
	"select-averageSalaryPerMonth" => "Penghasilan rata - rata / Bulan",
	"textarea-address" => "Alamat (Format : Alamat Block Nomor)",
	"input-rt" => "RT",
	"input-rw" => "RW",
	"select-village" => "Kelurahan / Desa",
	"select-district" => "Kecamatan",
	"select-regency" => "Kota",
	"select-province" => "Provinsi",
	"input-homeNumber" => "Nomor Tlp Rumah (Format : 0341-xxxx-xxxx)",
	"input-phoneNumber" => "Nomor Tlp Genggam (Format : 08xx-xxxx-xxxx)",
	"input-longStay" => "Lama tinggal di alamat tersebut",
	"textarea-otherAddress" => "Alamat sebelumnya (Format : Alamat Block Nomor, Keluraha Kecamatan, Kota, Provinsi)",
	"textarea-reasonOtherAddress" => "Alasan pindah alamat",
);

$lang["form_relationship"] = array(
	"input-nikFamily" => "Nomor KTP / Identitas Lain",
	"input-nameFamily" => "Nama Lengkap Keluarga",
	"select-relationFamily" => "Hubungan Keluarga",
	"select-occupationFamily" => "Pekerjaan / Usaha",
	"select-averageSalaryPerMonthFamily" => "Penghasilan rata - rata / Bulan",
	"input-amountDependent" => "Jumlah anak yang masih di tanggung",
);

$lang["form_otherData"] = array(
	"textarea-otherData" => "Data Pendukung / Lainnya (Catatan)",
);

$lang["notify_registration"] = array(
	"tnc" => array(
		"title" => "Persyaratan & Ketentuan Keanggotaan",
		"message" => "Persyaratan & Ketentuan Keanggotaan,<br><ul><li>Simpanan Pokok<br>%a</li><li>Simpanan Wajib<br>%b</li></ul>",
	),
	"success" => array(
		"title" => "Berhasil menjadi nasabah " . COMPANY,
		"message" => "Nasabah sudah bisa ke menu simpanan.",
	),
	"failed" => array(
		"title" => "Gagal Mendaftar Anggota Baru",
		"message" => "Terjadi kesalahan pada data.",
	),
);
