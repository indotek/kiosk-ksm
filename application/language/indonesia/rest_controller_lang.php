<?php

/*
 * English language
 */

defined('BASEPATH') OR exit('No direct script access allowed');

$lang['text_rest_invalid_api_key'] = 'Kunci API tidak valid %s'; // %s is the REST API key
$lang['text_rest_invalid_credentials'] = 'Kredensial tidak valid';
$lang['text_rest_ip_denied'] = 'IP ditolak';
$lang['text_rest_ip_unauthorized'] = 'IP tidak sah';
$lang['text_rest_unauthorized'] = 'Tidak resmi';
$lang['text_rest_ajax_only'] = 'Hanya permintaan AJAX yang diizinkan';
$lang['text_rest_api_key_unauthorized'] = 'Kunci API ini tidak memiliki akses ke pengontrol yang diminta';
$lang['text_rest_api_key_permissions'] = 'Kunci API ini tidak memiliki izin yang cukup';
$lang['text_rest_api_key_time_limit'] = 'Kunci API ini telah mencapai batas waktu untuk metode ini';
$lang['text_rest_ip_address_time_limit'] = 'Alamat IP ini telah mencapai batas waktu untuk metode ini';
$lang['text_rest_unknown_method'] = 'Metode tidak dikenal';
$lang['text_rest_unsupported'] = 'Protokol yang tidak didukung';

$lang["error_api"] = array(
	"0" => "BERHASIL",
	"-1000" => "DATA TIDAK DITEMUKAN",
	"-1001" => "DATA WAJIB DI ISI",
	"-1002" => "DATA SUDAH ADA",
	"-1003" => "ANDA BELUM MELAKUKAN SINKRONISASI.<br>MOHON MELAKUKAN SINKRONISASI TERLEBIH DAHULU UNTUK MENGURANGI KESALAHAN DAN MENDAPATKAN DATA TERKINI.",
	"-1004" => "TIDAK ADA PARAMETER",

	"-2001" => "GAGAL UNTUK MEMASUKKAN KE DALAM PENYIMPANAN DATA",
	"-3001" => "GAGAL UNTUK MELAKUKAN SINKRONISASI",
	"-3002" => "GAGAL UNTUK MELAKUKAN SINKRONISASI. KONEKSI TIDAK ADA.",
	"-3003" => "TIDAK ADA DATA YANG DI SINKRONISASI.",
);
