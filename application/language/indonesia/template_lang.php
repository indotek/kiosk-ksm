<?php
/**
 * Created by IntelliJ IDEA.
 * User: Technokrat
 * Date: 09/04/2020
 * Time: 08:02
 */

defined('BASEPATH') OR exit('No direct script access allowed');

$lang["menu"] = array(
	array(
		"type" => "link",
		"title" => "Beranda",
		"icon" => "fas fa-house-user",
		"target" => "dashboard",
	),
	array(
		"type" => "header",
		"title" => "Nasabah",
	),
	array(
		"type" => "link",
		"title" => "Daftar",
		"icon" => "fas fa-user-circle",
		"target" => "customer/registration"
	),
	array(
		"type" => "link",
		"title" => "Simpanan",
		"icon" => "fas fa-money-bill",
		"target" => "customer/search"
	),
	array(
		"type" => "header",
		"title" => "Pengaturan",
	),
//	array(
//		"type" => "link",
//		"title" => "Akun",
//		"icon" => "fas fa-tachometer-alt",
//		"target" => "test"
//	),
	array(
		"type" => "link",
		"title" => "Keluar",
		"icon" => "fas fa-sign-out-alt",
		"id" => "btnLogout"
	),
);

$lang["welcome"] = "<h2>Selamat Datang {user},</h2><span>Anda masuk sebagai {role} di Aplikasi Koperasi Sido Mandiri.</span>";


$lang["form"] = array(
	"select" => array(
		"placeholder" => "Pilih"
	),
	"phoneID" => "Masukkan nomor hp / telepon",
	"nikNotRegistered" => "NIK sudah terdaftar sebagai anggota.",
	"datepicker" => array(
		"monthsFull" => array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'),
		"monthsShort" => array('Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'),
		"weekdaysFull" => array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu'),
		"weekdaysShort" => array('Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'),
		"today" => 'Sekarang',
		"clear" => 'Bersih',
		"close" => 'Tutup',
		"format" => 'Pilih: dddd, dd mmmm yyyy',
  		"formatSubmit" => 'yyyy-mm-dd',
	)
);

$lang["popup"] = array(
	"logout" => array(
		"failed" => array(
			"title" => "Gagal untuk keluar.",
			"message" => "Terjadi Kesalahan pada service keluar.",
		)
	),
	"synchronize" => array(
		"failed" => array(
			"message" => "Gagal Sinkronisasi"
		),
		"success" => array(
			"message" => "Berhasil Sinkronisasi"
		),
		"nothing_to_sync" => array(
			"message" => "Tidak ada yang di Sinkronisasi"
		),
	),
	"button" => array(
		"btnYes" => "Ya",
		"btnNo" => "Tidak",
		"btnGagal" => "Gagal",
		"btnOk" => "Oke",
		"btnConfirm" => "Konfirmasi",
		"btnProcess" => "Proses",
	)
);

$lang["template-search-customer"] = array(
	"title" => "Pencarian Nasabah",
	"input-nik" => "Masukkan NIK Nasabah",
	"btnSearchCustomer" => "Cari",
	"nik-empty" => "NIK kosong, tidak dapat mencari.",
	"nik-lessthan-minlength" => "Panjang NIK kurang dari %s, tidak dapat mencari.",
	"popupMessage-nik-already" => "Berhasil menemukan nasabah<br>Apakah anda ingin melanjutkan untuk simpanan / pinjaman ?",
	"popupMessage-nik-not-found" => "NIK bukan nasabah<br>Apakah anda ingin melanjutkan untuk pendaftaran ?",
	"popupTitle-nik-already" => "NIK sudah terdaftar nasabah",
	"popupTitle-nik-not-found" => "NIK belum terdaftar nasabah",
);
